package launcher;

import com.google.gson.JsonObject;
import com.sun.javafx.application.PlatformImpl;
import javafx.stage.Stage;
import launcher.controllers.MenuController;
import launcher.layouts.LoginPage;
import launcher.layouts.MenuPage;
import launcher.layouts.UpdatePage;
import launcher.utils.PasswordHandler;

import javax.swing.filechooser.FileSystemView;
import java.io.File;
import java.io.IOException;

public class Main{
    public static void main(String[] args) {
        System.setProperty("prism.lcdtext", "false");
        File file = new File(FileSystemView.getFileSystemView().getDefaultDirectory().getPath() + "/GatariCraft/save.gc");
        if(file.exists()) {
            try {
                JsonObject jsonObject = new PasswordHandler().checkSavePassword(file);
                if(jsonObject.size() == 13) {
                    MenuController.user = jsonObject;
                    PlatformImpl.startup(()->{
                        try {
                            new UpdatePage().start(new Stage());
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    });
                } else {
                    file.delete();
                    PlatformImpl.startup(()->{
                        try {
                            new UpdatePage().start(new Stage());
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    });
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            PlatformImpl.startup(()->{
                try {
                    new UpdatePage().start(new Stage());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            });
        }
    }
}
