package launcher.api;

import com.google.gson.JsonObject;
import launcher.utils.JsonParser;

import java.io.IOException;

public class Clients {
    public JsonObject getClient(String login, String password) throws IOException {
        JsonObject jsonObject = new JsonParser().parseUrl("https://mc.gatari.pw:1488/clients/" + login + "/" + password);
        return jsonObject; 
    }
}
