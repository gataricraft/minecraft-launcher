package launcher.api;

import com.google.gson.JsonObject;
import launcher.config.Config;
import launcher.utils.JsonParser;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class GCAPIClients {

    public Map<String, String> getResponse(Map<String, String> args) throws IOException {
        JsonObject userJ = new JsonParser().parseUrl(Config.apiClientsUrl + args.get("login") + "/" + args.get("password"));
        Map<String, String> user = new HashMap<>();
        String[] keys = userJ.keySet().toArray(new String[0]);
        for(int i = 0; i < userJ.size(); i++) {
            user.put(keys[i], userJ.get(keys[i]).getAsString());
        }
        return user;
    }
}
