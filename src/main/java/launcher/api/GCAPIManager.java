package launcher.api;

import launcher.config.Config;
import launcher.config.User;

import java.io.IOException;
import java.util.Map;

public class GCAPIManager {
    public void manageRequest(String requestIndex, Map<String, String> args) throws GCAPIException, IOException {
        switch (requestIndex) {
            case Config.apiClientsRequestIndex:
                if (checkArguments(requestIndex, args)) {
                    User.user = new GCAPIClients().getResponse(args);
                    break;
                } else {
                    throw new GCAPIException();
                }
        }
    }

    private boolean checkArguments(String requestIndex, Map<String, String> args) {
        boolean b = false;
        switch (requestIndex) {
            case Config.apiClientsRequestIndex:
                if(args.containsKey("login") && args.containsKey("password")) b = true;
                break;
        }
        return b;
    }
}
