package launcher.config;

import javax.swing.filechooser.FileSystemView;

public class Config {
    public final static String gamePath = "C:/GatariCraft/";
    public final static String GCrepositoryUrl = "https://mc.gatari.pw/minecraft/";
    public final static String apiUrl = "https://mc.gatari.pw:1488/";
    public final static String apiAcessTokenUrl = apiUrl + "acess_token/";
    public final static String apiClientsUrl = apiUrl + "clients/";
    public final static String apiNewsUrl = apiUrl + "latest_news/";
    public final static String apiClientsRequestIndex = "clients";
    public final static String regex = "^[A-Za-z0-9 _\\\\-\\\\[\\\\]]{4,15}$";
    public final static String dataPath = FileSystemView.getFileSystemView().getDefaultDirectory().getPath() + "/GatariCraft/";
    public final static String settingsPath = dataPath + "settings.properties";
    public final static String sgcaat = "40ef0eb6-a2d9-4c87-b5ce-7cda598ae33c";
    public final static String launcherVersion = "Версия лаунчера - 0.6.0.0";
    public final static int socketPort = 35114;
    public final static int minMemory = 1024;
    public static int userMemory = 0;
    public static int memoryNow = 3072;
    public static int serverId = -1;
}
