package launcher.controllers;

import com.google.gson.JsonObject;
import com.sun.javafx.application.PlatformImpl;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import launcher.api.Clients;
import launcher.layouts.LoginPage;
import launcher.layouts.MenuPage;
import launcher.utils.Animations;
import launcher.utils.PasswordHandler;

import java.awt.*;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

public class LoginController {

    @FXML private AnchorPane body;
    @FXML private Pane border;
    @FXML private Hyperlink siteLink;
    @FXML private TextField loginField;
    @FXML private PasswordField passwordField;
    @FXML private Button loginButton;
    @FXML private Label error;

    private double xOffset;
    private double yOffset;

    @FXML
    public void initialize() {

        // Set eventHandler to pane
        border.setOnMousePressed(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                xOffset = LoginPage.stage.getX() - event.getScreenX();
                yOffset = LoginPage.stage.getY() - event.getScreenY();
            }
        });
        border.setOnMouseDragged(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                LoginPage.stage.setX(event.getScreenX() + xOffset);
                LoginPage.stage.setY(event.getScreenY() + yOffset);
            }
        });

        // Set action on hyperlink
        siteLink.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                try {
                    Desktop.getDesktop().browse(new URI("http://mc.gatari.pw/signup"));
                } catch (IOException | URISyntaxException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @FXML
    public void closeRequest() {
        System.exit(0);
    }

    @FXML
    public void minimizeRequest() {
        LoginPage.stage.setIconified(true);
    }

    @FXML
    public void login() {
        Thread t1 = new Thread(() -> {
            Platform.runLater(() -> {
                loginButton.setDisable(true);
                new Animations().animation(400, loginButton, 1.0, 0.7, false);
                passwordField.setDisable(true);
                new Animations().animation(400, passwordField, 1.0, 0.9, false);
                loginField.setDisable(true);
                new Animations().animation(400, loginField, 1.0, 0.9, false);
            });
            try {
                JsonObject jsonObject = new Clients().getClient(loginField.getText(), passwordField.getText());
                if(jsonObject.size() == 13) {
                    MenuController.user = jsonObject;
                    new PasswordHandler().hashPassAndSave(passwordField.getText(), loginField.getText());
                    Platform.runLater(() -> {
                        LoginPage.stage.close();
                    });
                    PlatformImpl.startup(()->{
                        try {
                            new MenuPage().start(new Stage());
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    });
                } else {
                    Platform.runLater(() -> {
                        error.setText("Wrong email/password combination");
                        new Animations().animation(400, error, 0.0, 1.0, false);
                    });
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            Platform.runLater(() -> {
                new Animations().animation(400, loginButton, 1.0, 0.7, false);
                new Animations().animation(400, passwordField, 1.0, 0.7, false);
                new Animations().animation(400, loginField, 1.0, 0.7, false);
                new Timer().schedule(new TimerTask() {
                    @Override
                    public void run() {
                        Platform.runLater(() -> {
                            loginButton.setDisable(false);
                            passwordField.setDisable(false);
                            loginField.setDisable(false);
                        });
                    }
                }, 400);
            });
        });
        if(!loginField.getText().trim().equals("")) {
            if(!passwordField.getText().trim().equals("")) {
                t1.start();
            } else {
                error.setText("The password is required");
                new Animations().animation(400, error, 0.0, 1.0, false);
            }
        } else {
            error.setText("The login is required");
            new Animations().animation(400, error, 0.0, 1.0, false);
        }
    }
}
