package launcher.controllers;

import com.google.gson.JsonObject;
import com.jfoenix.controls.JFXSlider;
import com.jfoenix.controls.JFXToggleButton;
import com.sun.management.OperatingSystemMXBean;
import javafx.animation.FadeTransition;
import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.util.Duration;
import launcher.download.DownloadMC;
import launcher.layouts.LoginPage;
import launcher.layouts.MenuPage;
import launcher.security.FileVerify;
import launcher.utils.Animations;

import javax.swing.filechooser.FileSystemView;
import java.io.*;
import java.lang.management.ManagementFactory;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Properties;
import java.util.Timer;
import java.util.TimerTask;

public class MenuController {

    public static int serverId = -1;
    public static JsonObject user = null;

    @FXML private AnchorPane anchorPane;
    @FXML private Pane settings;
    @FXML private Pane select;
    @FXML private Pane loading;
    @FXML private Pane border;
    @FXML private Pane serverPane1;
    @FXML private Pane serverPane2;
    @FXML private Pane serverPane3;
    @FXML private Label ram;
    @FXML private Label error;
    @FXML private Label fileLabel;
    @FXML private Label progressLabel;
    @FXML private Label settingsUserLoginLabel;
    @FXML private Button serverButton1;
    @FXML private Button serverButton2;
    @FXML private Button serverButton3;
    @FXML private Button saveButton;
    @FXML private Button leftButton;
    @FXML private Button rightButton;
    @FXML private JFXToggleButton overlayButton;
    @FXML private JFXSlider slider;
    @FXML private ProgressBar progress;

    private int memory;
    private ArrayList<Pane> selectPosition;
    private ArrayList<Button> serverSelectButtons;
    private double xOffset;
    private double yOffset;

    private String mainPath = "C:/GatariCraft/";

    @FXML
    public void initialize() {

        // Set eventHandler to pane
        border.setOnMousePressed(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                xOffset = MenuPage.stage.getX() - event.getScreenX();
                yOffset = MenuPage.stage.getY() - event.getScreenY();
            }
        });
        border.setOnMouseDragged(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                MenuPage.stage.setX(event.getScreenX() + xOffset);
                MenuPage.stage.setY(event.getScreenY() + yOffset);
            }
        });

        // Calculate PC memory
        OperatingSystemMXBean mxbean = (OperatingSystemMXBean) ManagementFactory.getOperatingSystemMXBean();
        memory = (int) (mxbean.getTotalPhysicalMemorySize()/1024/1024);

            // Set memory to setting slider
        slider.setMax(memory);
        ram.setText("1024мб / " + memory + "мб");

        // launcher.Loading variable settings to elements
        settingsLoad();

        // Add panes in position array
        selectPosition = new ArrayList<>();
        selectPosition.add(serverPane1);
        selectPosition.add(serverPane2);
        selectPosition.add(serverPane3);

        // Add buttons in select array
        serverSelectButtons = new ArrayList<>();
        serverSelectButtons.add(serverButton1);
        serverSelectButtons.add(serverButton2);
        serverSelectButtons.add(serverButton3);
    }

    @FXML
    public void closeRequest() {
        System.exit(0);
    }

    @FXML
    public void minimizeRequest() {
        MenuPage.stage.setIconified(true);
    }

    @FXML
    public void openSettings() {
        settingsLoad();
        settings.setVisible(true);
        new Animations().animation(400, settings, 0.0, 1.0, false);
    }

    @FXML
    public void saveSettings() {
        File settingsDir = new File(FileSystemView.getFileSystemView().getDefaultDirectory().getPath() + "/GatariCraft/");
        File settingsFile = new File(FileSystemView.getFileSystemView().getDefaultDirectory().getPath() + "/GatariCraft/settings.properties");
        if(settingsFile.exists()) {
            try {
                settingWrite(settingsFile);
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            try {
                settingsDir.mkdirs();
                if(!settingsFile.createNewFile()) System.out.println("Govno file ne mojet sohranit'sya");
                try {
                    settingWrite(settingsFile);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        saveButton.setText("Сохранено!");
        new Animations().animation(400, saveButton, 1.0, 0.7, false);
        saveButton.setDisable(true);
    }

    @FXML
    public void cancelSettings() {
        new Animations().animation(400, settings, 1.0, 0.0, false);
        new Timer().schedule(new TimerTask() {
            @Override
            public void run() {
                settings.setVisible(false);
            }
        }, 400);
    }

    @FXML
    public void ramEdit() {
        ram.setText((int)slider.getValue() + "мб / " + memory + "мб");
        settingsSaveButtonEdit();
    }

    @FXML
    public void overlayToggle() {
        settingsSaveButtonEdit();
    }

    @FXML
    public void openSelect() {
        select.setVisible(true);
        new Animations().animation(400, select, 0.0, 1.0, false);
    }

    @FXML
    public void cancelSelect() {
        new Animations().animation(400, select, 1.0, 0.0, false);
        new Timer().schedule(new TimerTask() {
            @Override
            public void run() {
                select.setVisible(false);
                Platform.runLater(() -> {
                    error.setText("");
                });
            }
        }, 400);
    }

    @FXML
    public void play() {
        if(serverId == -1) {
            error.setText("Вы не выбрали сервер!");
            select.setVisible(true);
            new Animations().animation(400, select, 0.0, 1.0, false);
        } else if(serverId == 0) {
            loading.setVisible(true);
            new Animations().animation(400, loading, 0.0, 1.0, false);
            new Thread(() -> {
                try {
                    FileVerify fv = new FileVerify(progress, fileLabel, progressLabel);
                    DownloadMC dmc = new DownloadMC(progress, fileLabel, progressLabel);
                    if(new File(mainPath + "1.12.2/").exists() || new File(mainPath + "HiTech/").exists()) {
                        fv.checkGameFiles("1.12.2", "1.12.2", "HiTech", "forge-14.23.5.2808", true);
                    } else {
                        dmc.downloadMinecraft("1.12.2", "1.12.2", false);
                        dmc.downloadAssets("1.12.2", "1.12.2");
                        dmc.downloadArray("1.12.2", "1.12.2", "libraries");
                        //dmc.downloadMinecraft("HiTech", "forge-14.23.5.2808", true);
                        dmc.downloadArray("HiTech", "forge-14.23.5.2808", "libraries");
                        dmc.downloadArray("HiTech", "forge-14.23.5.2808", "mods");
                    }
                } catch (IOException | NoSuchAlgorithmException e) {
                    e.printStackTrace();
                }
            }).start();
        } else if(serverId == 1) {
            loading.setVisible(true);
            new Animations().animation(400, loading, 0.0, 1.0, false);
        } else if(serverId == 2) {
            loading.setVisible(true);
            new Animations().animation(400, loading, 0.0, 1.0, false);
        }
    }

    @FXML
    public void selectServer1() {
        setServer(0);
    }

    @FXML
    public void selectServer2() {
        setServer(1);
    }

    @FXML
    public void selectServer3() {
        setServer(2);
    }

    @FXML
    public void selectLeft() {
        leftButton.setDisable(true);
        new Animations().animation(400, leftButton, 1.0, 0.7, false);
        Pane tempPane = null;
        Pane nextPane = null;
        int position = -1;
        for (int i = 0; i < selectPosition.size(); i++) {
            if (selectPosition.get(i).isVisible()) {
                tempPane = selectPosition.get(i);
                position = i;
            }
        }
        if(position != -1) {
            if(position == 0) {
                nextPane = selectPosition.get(selectPosition.size() - 1);
            } else {
                nextPane = selectPosition.get(position - 1);
            }
        }
        Pane finalTempPane = tempPane;
        new Animations().animation(400, finalTempPane, 1.0, 0.0, false);
        new Timer().schedule(new TimerTask() {
            @Override
            public void run() {
                finalTempPane.setVisible(false);
            }
        }, 400);
        Pane finalNextPane = nextPane;
        finalNextPane.setVisible(true);
        new Animations().animation(400, finalNextPane, 0.0, 1.0, false);
        new Animations().animation(400, leftButton, 0.7, 1.0, false);
        new Timer().schedule(new TimerTask() {
            @Override
            public void run() {
                leftButton.setDisable(false);
            }
        }, 400);
    }

    @FXML
    public void selectRight() {
        rightButton.setDisable(true);
        new Animations().animation(400, rightButton, 1.0, 0.7, false);
        Pane tempPane = null;
        Pane nextPane = null;
        int position = -1;
        for (int i = 0; i < selectPosition.size(); i++) {
            if (selectPosition.get(i).isVisible()) {
                tempPane = selectPosition.get(i);
                position = i;
            }
        }
        if(position != -1) {
            if(position == selectPosition.size()-1) {
                nextPane = selectPosition.get(0);
            } else {
                nextPane = selectPosition.get(position + 1);
            }
        }
        Pane finalTempPane = tempPane;
        new Animations().animation(400, finalTempPane, 1.0, 0.0, false);
        new Timer().schedule(new TimerTask() {
            @Override
            public void run() {
                finalTempPane.setVisible(false);
            }
        }, 400);
        Pane finalNextPane = nextPane;
        finalNextPane.setVisible(true);
        new Animations().animation(400, finalNextPane, 0.0, 1.0, false);
        new Animations().animation(400, rightButton, 0.7, 1.0, false);
        new Timer().schedule(new TimerTask() {
            @Override
            public void run() {
                rightButton.setDisable(false);
            }
        }, 400);
    }

    private void settingsSaveButtonEdit() {
        if(saveButton.isDisable()) {
            new Animations().animation(400, saveButton, 0.7, 1.0, false);
            new Timer().schedule(new TimerTask() {
                @Override
                public void run() {
                    Platform.runLater(() -> {
                        saveButton.setText("Сохранить");
                        saveButton.setDisable(false);
                    });
                }
            }, 400);
        }
    }

    private void settingWrite(File settingsFile) throws IOException {
        Properties properties = new Properties();
        properties.setProperty("RAM", String.valueOf((int)slider.getValue()));
        if(overlayButton.isSelected()) {
            properties.setProperty("OVERLAY", "TRUE");
        } else {
            properties.setProperty("OVERLAY", "FALSE");
        }
        properties.store(new FileOutputStream(settingsFile), "Settings");
    }

    private Properties propertiesLoad(File settingsFile) throws IOException {
        Properties properties = new Properties();
        properties.load(new FileInputStream(settingsFile));
        return properties;
    }

    private void settingsLoad() {
        // Set character name
        settingsUserLoginLabel.setText(user.get("login").getAsString());

        File settingsFile = new File(FileSystemView.getFileSystemView().getDefaultDirectory().getPath() + "/GatariCraft/settings.properties");
        try {
            if(settingsFile.exists()) {
                Properties properties = propertiesLoad(settingsFile);
                slider.setValue(Double.parseDouble(properties.getProperty("RAM")));
                ram.setText(Integer.parseInt(properties.getProperty("RAM")) + "мб / " + memory + "мб");
                overlayButton.setSelected(Boolean.parseBoolean(properties.getProperty("OVERLAY")));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void setServer(int i) {
        serverSelectButtons.get(i).setDisable(true);
        if(serverId == i) {
            serverSelectButtons.get(i).setText("Выбрать сервер");
            new Animations().animation(400, serverSelectButtons.get(i), 0.7, 1.0, false);
            serverId = -1;
        } else {
            serverSelectButtons.get(i).setText("Сервер выбран!");
            new Animations().animation(400, serverSelectButtons.get(i), 1.0, 0.7, false);
            serverId = i;
            for(int j = 0; j < serverSelectButtons.size(); j++) {
                Button tempButton = serverSelectButtons.get(j);
                if(tempButton.getOpacity() == 0.7 && tempButton.getText().equals("Сервер выбран!")) {
                    tempButton.setText("Выбрать сервер");
                    tempButton.setOpacity(1.0);
                }
            }
        }
        serverSelectButtons.get(i).setDisable(false);
    }
}
