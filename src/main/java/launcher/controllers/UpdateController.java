package launcher.controllers;

import com.sun.management.OperatingSystemMXBean;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import launcher.download.LauncherUpdate;
import launcher.layouts.MenuPage;
import launcher.layouts.UpdatePage;

import java.io.File;
import java.io.IOException;
import java.lang.management.ManagementFactory;
import java.net.URISyntaxException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;

public class UpdateController {

    @FXML private AnchorPane anchorPane;
    @FXML private Pane border;
    @FXML private Label fileLabel;
    @FXML private Label progressLabel;
    @FXML private ProgressBar progress;

    private double xOffset;
    private double yOffset;

    @FXML
    public void initialize() {

        // Set eventHandler to pane
        border.setOnMousePressed(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                xOffset = UpdatePage.stage.getX() - event.getScreenX();
                yOffset = UpdatePage.stage.getY() - event.getScreenY();
            }
        });
        border.setOnMouseDragged(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                UpdatePage.stage.setX(event.getScreenX() + xOffset);
                UpdatePage.stage.setY(event.getScreenY() + yOffset);
            }
        });
        new Thread(() -> {
            try {
                new LauncherUpdate(progressLabel, fileLabel, progress).checkUpdate();
            } catch (IOException | URISyntaxException | NoSuchAlgorithmException e) {
                e.printStackTrace();
            }
        }).start();
    }

    @FXML
    public void closeRequest() {
        System.exit(0);
    }

    @FXML
    public void minimizeRequest() {
        UpdatePage.stage.setIconified(true);
    }
}
