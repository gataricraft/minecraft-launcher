package launcher.download;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.jfoenix.controls.JFXSlider;
import javafx.application.Platform;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import launcher.controllers.MenuController;
import launcher.security.FileVerify;
import launcher.utils.JsonParser;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.NoSuchAlgorithmException;

public class DownloadMC {

    private ProgressBar progress;
    private Label fileLabel;
    private Label progressLabel;
    private String mainUrl = "https://mc.gatari.pw/minecraft/";
    private String mainPath = "C:/GatariCraft/";

    public DownloadMC(ProgressBar progress, Label fileLabel, Label progressLabel) {
        this.progress = progress;
        this.fileLabel = fileLabel;
        this.progressLabel = progressLabel;
    }

    public void downloadAssets(String packId, String subVersionId) throws IOException, NoSuchAlgorithmException {
        // Donwload assets
        Platform.runLater(() -> {
            fileLabel.setText("indexes parsing");
        });
        JsonObject json = new JsonParser().parseUrl(mainUrl + packId + "/versions/" + subVersionId + "/" + subVersionId + ".json");
        JsonObject assetIndex = json.get("assetIndex").getAsJsonObject();
        JsonObject assetObjects = new JsonParser().parseUrl(assetIndex.get("url").getAsString()).get("objects").getAsJsonObject();
        String[] keys = assetObjects.keySet().toArray(new String[0]);
        for(int i = 0; i < assetObjects.size(); i++) {
            String  hash = assetObjects.get(keys[i]).getAsJsonObject().get("hash").getAsString();
            long  size = assetObjects.get(keys[i]).getAsJsonObject().get("size").getAsInt();
            String path = mainPath + packId + "/assets/objects/" + hash.substring(0, 2) + "/";
            File file = new File(path);
            if(!file.exists()) file.mkdirs();
            downloadFile(mainUrl + packId + "/assets/" + keys[i], path, hash, size);
        }
        // Download assetsIndex
        File assetIndexDir = new File(mainPath + packId +  "/assets/indexes/");
        if(!assetIndexDir.exists()) assetIndexDir.mkdirs();
        downloadFile(assetIndex.get("url").getAsString(),mainPath + packId +  "/assets/indexes/", subVersionId + ".json", assetIndex.get("size").getAsLong());
        //Download logConfig
        JsonObject logConfig = json.get("logging").getAsJsonObject().get("client").getAsJsonObject().get("file").getAsJsonObject();
        File logConfigDir = new File(mainPath + packId +  "/assets/log_configs/");
        if(!logConfigDir.exists()) logConfigDir.mkdirs();
        downloadFile(logConfig.get("url").getAsString(), mainPath + packId +  "/assets/log_configs/", logConfig.get("id").getAsString(), logConfig.get("size").getAsLong());
    }

    public void downloadArray(String packId, String subVersionId, String arrayName) throws IOException, NoSuchAlgorithmException {
        Platform.runLater(() -> {
            fileLabel.setText("indexes parsing");
        });
        JsonObject json = new JsonParser().parseUrl(mainUrl + packId + "/versions/" + subVersionId + "/" + subVersionId + ".json");
        JsonArray libs = json.get(arrayName).getAsJsonArray();
        for(int i = 0; i < libs.size(); i++) {
            JsonObject downloads = libs.get(i).getAsJsonObject().get("downloads").getAsJsonObject();
            if(downloads.get("artifact") != null) {
                JsonObject temp = downloads.get("artifact").getAsJsonObject();
                String name = temp.get("path").getAsString().split("/")[temp.get("path").getAsString().split("/").length - 1];
                String path = mainPath + packId + "/" + arrayName + temp.get("path").getAsString().replace(name, "");
                File libDir = new File(path);
                if(!libDir.exists()) libDir.mkdirs();
                downloadFile(temp.get("url").getAsString(), path, name, temp.get("size").getAsLong());
            }
            if(downloads.get("classifiers") != null) {
                if(downloads.get("classifiers").getAsJsonObject().get("natives-windows") != null) {
                    JsonObject temp = downloads.get("classifiers").getAsJsonObject().get("natives-windows").getAsJsonObject();
                    String name =  temp.get("path").getAsString().split("/")[temp.get("path").getAsString().split("/").length - 1];
                    String path = mainPath + packId + "/" + arrayName + temp.get("path").getAsString().replace(name, "");
                    File libDir = new File(path);
                    if(!libDir.exists()) libDir.mkdirs();
                    downloadFile(temp.get("url").getAsString(), path, name, temp.get("size").getAsLong());
                }
            }
        }
    }

    public void downloadMinecraft(String packId, String subVersionId, boolean forge) throws IOException {
        Platform.runLater(() -> {
            fileLabel.setText("indexes parsing");
        });
        JsonObject json = new JsonParser().parseUrl(mainUrl + packId + "/versions/" + subVersionId + "/" + subVersionId + ".json");
        if(!forge) {
            JsonObject temp = json.get("downloads").getAsJsonObject().get("client").getAsJsonObject();
            String path = mainPath + packId + "/versions/" + subVersionId + "/";
            File jarDir = new File(path);
            if(!jarDir.exists()) jarDir.mkdirs();
            String name = subVersionId + ".jar";
            downloadFile(temp.get("url").getAsString(), path, name, temp.get("size").getAsLong());
            File indexFile = new File(mainPath + packId + "/versions/" + subVersionId + "/" + subVersionId + ".json");
            if(!indexFile.exists()) {
                new File(mainPath + packId + "/versions/" + subVersionId + "/").mkdirs();
                indexFile.createNewFile();
            }
            FileWriter fileWriter = new FileWriter(indexFile);
            fileWriter.write(json.toString());
            fileWriter.close();
        } else {
            File indexFile = new File(mainPath + packId + "/versions/" + subVersionId + "/" + subVersionId + ".json");
            if(!indexFile.exists()) {
                new File(mainPath + packId + "/versions/" + subVersionId + "/").mkdirs();
                indexFile.createNewFile();
            }
            FileWriter fileWriter = new FileWriter(indexFile);
            fileWriter.write(json.toString());
            fileWriter.close();
        }
    }

    private void downloadFile(String site, String path, String fileName, long fileSize) throws IOException {
        URL url = new URL(site);
        HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
        BufferedInputStream in = new BufferedInputStream(httpURLConnection.getInputStream());
        new File(path).mkdirs();
        FileOutputStream fos = new FileOutputStream(path + fileName);
        BufferedOutputStream bout = new BufferedOutputStream(fos, 1024);
        byte[] data = new byte[1024];
        long downloadedFileSize = 0;
        int x = 0;
        Platform.runLater(() -> {
            fileLabel.setText("loading " + fileName);
        });
        while ((x = in.read(data, 0, 1024)) >= 0) {
            downloadedFileSize += x;

            final double curretProgress =((((double)downloadedFileSize) / ((double)fileSize)));

            Platform.runLater(() -> {
                progress.setProgress(curretProgress);
                progressLabel.setText(String.valueOf((int)(curretProgress * 100)));
            });

            bout.write(data, 0, x);
        }
        bout.close();
        in.close();
    }

}
