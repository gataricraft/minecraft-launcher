package launcher.download;

import com.google.gson.JsonObject;
import com.sun.javafx.application.PlatformImpl;
import javafx.application.Platform;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.stage.Stage;
import launcher.controllers.MenuController;
import launcher.layouts.LoginPage;
import launcher.layouts.MenuPage;
import launcher.layouts.UpdatePage;
import launcher.utils.JsonParser;
import launcher.utils.SHA1;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URISyntaxException;
import java.net.URL;
import java.security.NoSuchAlgorithmException;

public class LauncherUpdate {

    private Label fileLabel;
    private Label progressLabel;
    private ProgressBar progress;
    private String mainUrl = "https://mc.gatari.pw/minecraft/";
    private String mainPath = "C:/GatariCraft/";

    public LauncherUpdate(Label progressLabel, Label fileLabel, ProgressBar progress) {
        this.fileLabel = fileLabel;
        this.progressLabel = progressLabel;
        this.progress = progress;
    }

    public void checkUpdate() throws IOException, URISyntaxException, NoSuchAlgorithmException {
        Platform.runLater(() -> {
            fileLabel.setText("checking launcher update...");
        });
        JsonObject json = new JsonParser().parseUrl(mainUrl + "launcher/launcher.json");
        File launcher = new File(UpdatePage.class.getProtectionDomain().getCodeSource().getLocation().toURI().getPath());
        if(!json.get("sha1").getAsString().equals(SHA1.calcSHA1(launcher))) {
            update(launcher, json.get("name").getAsString(), json.get("size").getAsLong());
        } else {
            Platform.runLater(() -> {
                UpdatePage.stage.close();
            });
            if(MenuController.user == null) {
                PlatformImpl.startup(()->{
                    try {
                        new LoginPage().start(new Stage());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                });
            } else {
                PlatformImpl.startup(()->{
                    try {
                        new MenuPage().start(new Stage());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                });
            }
        }
    }

    private void update(File launcher, String fileName, long size) throws IOException {
        String name = launcher.getName();
        String path = launcher.getAbsolutePath().replace(name, "");
        downloadFile(mainUrl + "launcher/" + fileName, path, launcher.getName(), size);
        ProcessBuilder pb = new ProcessBuilder("java", "-jar", path + launcher.getName());
        pb.directory(new File(path));
        pb.start();
        System.exit(0);
    }

    private void downloadFile(String site, String path, String fileName, long fileSize) throws IOException {
        URL url = new URL(site);
        HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
        BufferedInputStream in = new BufferedInputStream(httpURLConnection.getInputStream());
        new File(path).mkdirs();
        FileOutputStream fos = new FileOutputStream(path + fileName);
        BufferedOutputStream bout = new BufferedOutputStream(fos, 1024);
        byte[] data = new byte[1024];
        long downloadedFileSize = 0;
        int x = 0;
        Platform.runLater(() -> {
            fileLabel.setText("loading new launcher...");
        });
        while ((x = in.read(data, 0, 1024)) >= 0) {
            downloadedFileSize += x;

            final double curretProgress =((((double)downloadedFileSize) / ((double)fileSize)));

            Platform.runLater(() -> {
                progress.setProgress(curretProgress);
                progressLabel.setText(String.valueOf((int)(curretProgress * 100)));
            });

            bout.write(data, 0, x);
        }
        bout.close();
        in.close();
    }

}
