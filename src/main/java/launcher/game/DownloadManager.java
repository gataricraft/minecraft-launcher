package launcher.game;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import javafx.application.Platform;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import launcher.game.config.BaseConfig;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;

public class DownloadManager {

    protected Label fileLabel;
    protected Label progressLabel;
    protected ProgressBar progress;

    public DownloadManager(Label fileLabel, Label progressLabel, ProgressBar progress) {
        this.fileLabel = fileLabel;
        this.progressLabel = progressLabel;
        this.progress = progress;
    }

    void downloadNotRequirementFiles(JsonObject files) throws IOException {
        JsonArray filesA = files.get("files").getAsJsonArray();
        for(JsonElement jsonElement : filesA) {
            JsonObject jsonObject = jsonElement.getAsJsonObject();
            String fullPath = BaseConfig.gameDir + jsonObject.get("path").getAsString();
            String name = fullPath.split("/")[fullPath.split("/").length-1];
            String path = fullPath.replace(name, "");
            String url = jsonObject.get("url").getAsString();
            downloadFile(url, path, name, getFileSize(url));
        }
    }

    void downloadForgeSingleFiles(JsonObject indexes) throws IOException {
        String clientJarName = BaseConfig.versionId + ".jar";
        String clientJarPath = BaseConfig.gameDir + BaseConfig.versionsFolder + "/" + BaseConfig.versionId + "/";
        String clientJarUrl = indexes.get("downloads").getAsJsonObject().get("client").getAsJsonObject().get("url").getAsString();
        long clientJarSize = indexes.get("downloads").getAsJsonObject().get("client").getAsJsonObject().get("size").getAsLong();
        downloadFile(clientJarUrl, clientJarPath, clientJarName, clientJarSize);

        String assetIndexFileName = indexes.get("assetIndex").getAsJsonObject().get("id").getAsString() + ".json";
        String assetIndexFilePath = BaseConfig.assetIndexesDir;
        String assetIndexFileUrl = indexes.get("assetIndex").getAsJsonObject().get("url").getAsString();
        long assetIndexFileLong = indexes.get("assetIndex").getAsJsonObject().get("size").getAsLong();
        downloadFile(assetIndexFileUrl, assetIndexFilePath, assetIndexFileName, assetIndexFileLong);
    }

    void downloadAssets(JsonObject assetsIndex) throws IOException {
        JsonObject assetsObject = assetsIndex.get("objects").getAsJsonObject();
        String[] keys = assetsObject.keySet().toArray(new String[0]);
        for(int i = 0; i < assetsObject.size(); i++) {
            JsonObject asset = assetsObject.get(keys[i]).getAsJsonObject();
            String name = asset.get("hash").getAsString();
            String url = BaseConfig.assetsRepository + name.substring(0, 2) + "/" + name;
            String path = BaseConfig.assetObjectsDir + name.substring(0, 2) + "/";
            long size = asset.get("size").getAsLong();
            downloadFile(url, path, name, size);
        }
    }

    void downloadNotOriginalArray(JsonArray jsonArray) throws IOException {
        for (JsonElement jsonElement : jsonArray) {
            JsonObject jsonObject = jsonElement.getAsJsonObject();
            String parsedName = parseLibName(jsonObject.get("name").getAsString());
            String url = null;
            if(jsonObject.get("url") != null) {
                url = jsonObject.get("url").getAsString() + parsedName;
            } else {
                url = BaseConfig.librariesRepository + parsedName;
            }
            String fullPath = BaseConfig.librariesDir + parsedName;
            String name = fullPath.split("/")[fullPath.split("/").length-1];
            String path = fullPath.replace(name, "");
            long size = getFileSize(url);
            downloadFile(url, path, name, size);
        }
    }

    void downloadOriginalArray(JsonArray jsonArray) throws IOException {
        for (JsonElement jsonElement : jsonArray) {
            JsonElement artifactElement = jsonElement.getAsJsonObject().get("downloads").getAsJsonObject().get("artifact");
            JsonElement classifiersElement = jsonElement.getAsJsonObject().get("downloads").getAsJsonObject().get("classifiers");

            if(artifactElement != null) {
                JsonObject artifact = artifactElement.getAsJsonObject();
                String url = artifact.get("url").getAsString();
                String fullPath = BaseConfig.librariesDir + artifact.get("path").getAsString();
                String path = fullPath.replace(fullPath.split("/")[fullPath.split("/").length-1], "");
                String name = fullPath.split("/")[fullPath.split("/").length-1];
                long size = artifact.get("size").getAsLong();
                downloadFile(url, path, name, size);
            }
            if(classifiersElement != null) {
                if(classifiersElement.getAsJsonObject().get("natives-windows") != null) {
                    JsonObject classifiers = classifiersElement.getAsJsonObject().get("natives-windows").getAsJsonObject();
                    String url = classifiers.get("url").getAsString();
                    String fullPath = BaseConfig.librariesDir + classifiers.get("path").getAsString();
                    String path = fullPath.replace(fullPath.split("/")[fullPath.split("/").length-1], "");
                    String name = fullPath.split("/")[fullPath.split("/").length-1];
                    long size = classifiers.get("size").getAsLong();
                    downloadFile(url, path, name, size);
                }
            }
        }
    }

    void downloadGCArray(JsonArray jsonArray) throws IOException {
        for (JsonElement jsonElement : jsonArray) {
            JsonObject jsonObject = jsonElement.getAsJsonObject();
            String fullPath = BaseConfig.gameDir + jsonObject.get("path").getAsString();
            String name = fullPath.split("/")[fullPath.split("/").length-1];
            String path = fullPath.replace(name, "");
            String url = jsonObject.get("url").getAsString();
            long size = getFileSize(url);
            downloadFile(url, path, name, size);
        }
    }

    public String parseLibName(String name) {
        StringBuilder link = new StringBuilder();
        String[] temp = name.split(":");
        String[] packages = temp[0].split("\\.");
        StringBuilder fileName = new StringBuilder();
        for(String string : packages) {
            link.append(string);
            link.append("/");
        }
        for(int i = 0; i < temp.length; i++) {
            if(i != 0) {
                link.append(temp[i]);
                link.append("/");
                fileName.append(temp[i]);
                fileName.append("-");
            }
        }
        fileName.setLength(fileName.length()-1);
        link.append(fileName.toString());
        link.append(".jar");
        return link.toString();
    }

    protected long getFileSize(String string) throws IOException {
        URL url = new URL(string);
        URLConnection urlConnection = url.openConnection();
        urlConnection.connect();
        return urlConnection.getContentLength();
    }

    public void downloadFile(String downloadUrl, String path, String fileName, long fileSize) throws IOException {
        URL url = new URL(downloadUrl);
        HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
        BufferedInputStream in = new BufferedInputStream(httpURLConnection.getInputStream());
        new File(path).mkdirs();
        FileOutputStream fos = new FileOutputStream(path + fileName);
        BufferedOutputStream bout = new BufferedOutputStream(fos, 1024);
        byte[] data = new byte[1024];
        long downloadedFileSize = 0;
        int x = 0;
        Platform.runLater(() -> {
            fileLabel.setText("loading " + fileName);
        });
        while ((x = in.read(data, 0, 1024)) >= 0) {
            downloadedFileSize += x;
            final double curretProgress =((((double)downloadedFileSize) / ((double)fileSize)));
            Platform.runLater(() -> {
                progress.setProgress(curretProgress);
                progressLabel.setText(String.valueOf((int)(curretProgress * 100)));
            });
            bout.write(data, 0, x);
        }
        bout.close();
        in.close();
    }
}
