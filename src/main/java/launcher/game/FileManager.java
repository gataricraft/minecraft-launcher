package launcher.game;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import javafx.application.Platform;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import launcher.game.config.BaseConfig;
import launcher.utils.SHA1;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class FileManager extends DownloadManager {

    ArrayList<List> localFiles;
    ArrayList<List> globalFies;

    public FileManager(Label fileLabel, Label progressLabel, ProgressBar progress) {
        super(fileLabel, progressLabel, progress);
    }

    public void verifyAllFiles(JsonArray notOriginalArray, JsonArray originalArray, JsonObject assetsIndexes, JsonObject indexes, JsonArray gcArray) throws IOException, NoSuchAlgorithmException {
        localFiles = new ArrayList<>();
        globalFies = new ArrayList<>();

        Platform.runLater(() -> fileLabel.setText("parse local files..."));
        if(new File(BaseConfig.librariesDir).listFiles() != null) {
            parseFolder(new File(BaseConfig.librariesDir).listFiles());
        }
        if(new File(BaseConfig.assetDir).listFiles() != null) {
            parseFolder(new File(BaseConfig.assetDir).listFiles());
        }
        if(new File(BaseConfig.gameDir + BaseConfig.versionsFolder + "/").listFiles() != null) {
            parseFolder(new File(BaseConfig.gameDir + BaseConfig.versionsFolder + "/").listFiles());
        }
        if(new File(BaseConfig.gameDir + "mods/").listFiles() != null) {
            parseFolder(new File(BaseConfig.gameDir + "mods/").listFiles());
        }

        Platform.runLater(() -> fileLabel.setText("parse global files..."));
        parseForgeSingleFiles(indexes);
        parseNotOriginalArray(notOriginalArray);
        parsetOriginalArray(originalArray);
        parseGCArray(gcArray);
        parseAssets(assetsIndexes);

        ArrayList<ArrayList> tempArray = (ArrayList<ArrayList>) globalFies.clone();
        ArrayList<ArrayList> tempArray1 = (ArrayList<ArrayList>) localFiles.clone();

        Platform.runLater(() -> fileLabel.setText("file comparison..."));

        for (List<String> fileArray : globalFies) {
            List<String> blankArray = new ArrayList<>();
            blankArray.add(fileArray.get(0));
            blankArray.add(fileArray.get(1));
            blankArray.add(fileArray.get(2));
            if(localFiles.contains(blankArray)) {
                tempArray.remove(fileArray);
                tempArray1.remove(blankArray);
            }
        }

        Platform.runLater(() -> fileLabel.setText("checking bad files..."));

        for(List<String> fileArray : tempArray1) {
            if(!new File(fileArray.get(0)).delete()) System.exit(-100);
        }

        Platform.runLater(() -> fileLabel.setText("checking files for download..."));

        for(List<String> fileArray :  tempArray) {
            String name = fileArray.get(0).split("/")[fileArray.get(0).split("/").length-1];
            String path = fileArray.get(0).replace(name, "");
            downloadFile(fileArray.get(3), path, name, Long.parseLong(fileArray.get(2)));
        }

        Platform.runLater(() -> fileLabel.setText("running game..."));

        globalFies.clear();
        localFiles.clear();
    }

    private void parseNotOriginalArray(JsonArray jsonArray) throws IOException {
        for (JsonElement jsonElement : jsonArray) {
            JsonObject jsonObject = jsonElement.getAsJsonObject();
            String parsedName = parseLibName(jsonObject.get("name").getAsString());
            String url = null;
            if(jsonObject.get("url") != null) {
                url = jsonObject.get("url").getAsString() + parsedName;
            } else {
                url = BaseConfig.librariesRepository + parsedName;
            }
            String fullPath = BaseConfig.librariesDir + parsedName;
            String name = fullPath.split("/")[fullPath.split("/").length-1];
            String path = fullPath.replace(name, "");
            long size = getFileSize(url);
            List<String> fileArray = new ArrayList<>();
            fileArray.add(path + name);
            fileArray.add(jsonObject.get("sha1").getAsString());
            fileArray.add(String.valueOf(size));
            fileArray.add(url);
            globalFies.add(fileArray);
        }
    }

    private void parsetOriginalArray(JsonArray jsonArray) throws IOException {
        for (JsonElement jsonElement : jsonArray) {
            JsonElement artifactElement = jsonElement.getAsJsonObject().get("downloads").getAsJsonObject().get("artifact");
            JsonElement classifiersElement = jsonElement.getAsJsonObject().get("downloads").getAsJsonObject().get("classifiers");

            if(artifactElement != null) {
                JsonObject artifact = artifactElement.getAsJsonObject();
                String url = artifact.get("url").getAsString();
                String fullPath = BaseConfig.librariesDir + artifact.get("path").getAsString();
                String path = fullPath.replace(fullPath.split("/")[fullPath.split("/").length-1], "");
                String name = fullPath.split("/")[fullPath.split("/").length-1];
                long size = artifact.get("size").getAsLong();
                List<String> artifactArray = new ArrayList<>();
                artifactArray.add(path + name);
                artifactArray.add(readSHA1(url + ".sha1"));
                artifactArray.add(String.valueOf(size));
                artifactArray.add(url);
                globalFies.add(artifactArray);
            }
            if(classifiersElement != null) {
                if(classifiersElement.getAsJsonObject().get("natives-windows") != null) {
                    JsonObject classifiers = classifiersElement.getAsJsonObject().get("natives-windows").getAsJsonObject();
                    String url = classifiers.get("url").getAsString();
                    String fullPath = BaseConfig.librariesDir + classifiers.get("path").getAsString();
                    String path = fullPath.replace(fullPath.split("/")[fullPath.split("/").length-1], "");
                    String name = fullPath.split("/")[fullPath.split("/").length-1];
                    long size = classifiers.get("size").getAsLong();
                    List<String> classifiersArray = new ArrayList<>();
                    classifiersArray.add(path + name);
                    classifiersArray.add(readSHA1(url + ".sha1"));
                    classifiersArray.add(String.valueOf(size));
                    classifiersArray.add(url);
                    globalFies.add(classifiersArray);
                }
            }
        }
    }

    private void parseGCArray(JsonArray jsonArray) throws IOException {
        for (JsonElement jsonElement : jsonArray) {
            JsonObject jsonObject = jsonElement.getAsJsonObject();
            String fullPath = BaseConfig.gameDir + jsonObject.get("path").getAsString();
            String name = fullPath.split("/")[fullPath.split("/").length-1];
            String path = fullPath.replace(name, "");
            String url = jsonObject.get("url").getAsString();
            long size = getFileSize(url);
            List<String> gcArray = new ArrayList<>();
            gcArray.add(path + name);
            gcArray.add(jsonObject.get("sha1").getAsString());
            gcArray.add(String.valueOf(size));
            gcArray.add(url);
            globalFies.add(gcArray);
        }
    }

    private void parseAssets(JsonObject assetsIndex) {
        JsonObject assetsObject = assetsIndex.get("objects").getAsJsonObject();
        String[] keys = assetsObject.keySet().toArray(new String[0]);
        for(int i = 0; i < assetsObject.size(); i++) {
            JsonObject asset = assetsObject.get(keys[i]).getAsJsonObject();
            String name = asset.get("hash").getAsString();
            String url = BaseConfig.assetsRepository + name.substring(0, 2) + "/" + name;
            String path = BaseConfig.assetObjectsDir + name.substring(0, 2) + "/";
            long size = asset.get("size").getAsLong();
            List<String> fileArray = new ArrayList<>();
            fileArray.add(path + name);
            fileArray.add(name);
            fileArray.add(String.valueOf(size));
            fileArray.add(url);
            globalFies.add(fileArray);
        }
    }

    private void parseForgeSingleFiles(JsonObject indexes) {
        String clientJarName = BaseConfig.versionId + ".jar";
        String clientJarPath = BaseConfig.gameDir + BaseConfig.versionsFolder + "/" + BaseConfig.versionId + "/";
        String clientJarUrl = indexes.get("downloads").getAsJsonObject().get("client").getAsJsonObject().get("url").getAsString();
        long clientJarSize = indexes.get("downloads").getAsJsonObject().get("client").getAsJsonObject().get("size").getAsLong();
        List<String> fileArray = new ArrayList<>();
        fileArray.add(clientJarPath + clientJarName);
        fileArray.add(indexes.get("downloads").getAsJsonObject().get("client").getAsJsonObject().get("sha1").getAsString());
        fileArray.add(String.valueOf(clientJarSize));
        fileArray.add(clientJarUrl);
        globalFies.add(fileArray);

        String assetIndexFileName = indexes.get("assetIndex").getAsJsonObject().get("id").getAsString() + ".json";
        String assetIndexFilePath = BaseConfig.assetIndexesDir;
        String assetIndexFileUrl = indexes.get("assetIndex").getAsJsonObject().get("url").getAsString();
        long assetIndexFileLong = indexes.get("assetIndex").getAsJsonObject().get("size").getAsLong();
        List<String> fileArray1 = new ArrayList<>();
        fileArray1.add(assetIndexFilePath + assetIndexFileName);
        fileArray1.add(indexes.get("assetIndex").getAsJsonObject().get("sha1").getAsString());
        fileArray1.add(String.valueOf(assetIndexFileLong));
        fileArray1.add(assetIndexFileUrl);
        globalFies.add(fileArray1);
    }

    private void parseFolder(File[] files) throws IOException, NoSuchAlgorithmException {
        for(File file: files) {
            if(!file.isDirectory()) {
                List<String> fileArray = new ArrayList<>();
                fileArray.add(file.getAbsolutePath().replace("\\", "/"));
                fileArray.add(SHA1.calcSHA1(file));
                fileArray.add(String.valueOf(file.length()));
                localFiles.add(fileArray);
            } else {
                parseFolder(Objects.requireNonNull(file.listFiles()));
            }
        }
    }

    private String readSHA1(String site) throws IOException {
        URL url = new URL(site);
        BufferedReader in = new BufferedReader(new InputStreamReader(url.openStream()));
        String inputLine;
        StringBuilder stringBuilder = new StringBuilder();
        while ((inputLine = in.readLine()) != null)
            stringBuilder.append(inputLine);
        in.close();
        return stringBuilder.toString();
    }
}
