package launcher.game;

import launcher.config.Config;
import launcher.config.User;
import launcher.game.config.BaseConfig;
import launcher.utils.JsonParser;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class GameArgumentsHandler {
    public List<String> parseArguments(String rawArgs, boolean forge) throws IOException {
        String[] args = rawArgs.split(" ");
        List<String> returnArgs = new ArrayList<>();
        Map<String, String> vArgs = getArgs(forge);
        for(String arg : args) {
            if(vArgs.containsKey(arg)) {
                returnArgs.add(arg);
                returnArgs.add(vArgs.get(arg));
            }
        }
        return returnArgs;
    }

    private Map<String, String> getArgs(boolean forge) throws IOException {
        String accessToken = new JsonParser().parseUrl(Config.apiUrl + "get_access_token/" + User.user.get("login") + "/" + User.user.get("uuid") + "/" + Config.sgcaat).get("accessToken").getAsString();
        Map<String, String> args = new HashMap<>();
        args.put("--username", User.user.get("login"));
        args.put("--version", BaseConfig.versionId);
        args.put("--gameDir", BaseConfig.gameDir);
        args.put("--assetsDir", BaseConfig.assetDir);
        args.put("--assetIndex", BaseConfig.assetIndex);
        args.put("--uuid", User.user.get("uuid"));
        args.put("--accessToken", accessToken);
        args.put("--userType", "mojang");
        if(forge) {
            args.put("--tweakClass", "net.minecraftforge.fml.common.launcher.FMLTweaker");
            args.put("--versionType", "Forge");
        } else {
            args.put("--versionType", "release");
        }
        return args;
    }
}
