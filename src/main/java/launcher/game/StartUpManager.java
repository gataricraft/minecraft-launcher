package launcher.game;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import javafx.application.Platform;
import launcher.game.config.BaseConfig;
import launcher.ui.layouts.MenuPage;
import launcher.utils.JsonParser;
import net.lingala.zip4j.exception.ZipException;

import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.util.List;

public class StartUpManager {

    protected static FileManager fileManager;

    public StartUpManager(FileManager fileManager) {
        StartUpManager.fileManager = fileManager;
    }

    public void startUpManagerRequest(int serverId) {
        new Thread(() -> {
            new BaseConfig().setConfig(serverId);
            try {
                readIndexes(BaseConfig.forge);
            } catch (IOException | NoSuchAlgorithmException | ZipException | InterruptedException e) {
                e.printStackTrace();
            }
        }).start();
    }

    private void readIndexes(boolean forge) throws IOException, NoSuchAlgorithmException, ZipException, InterruptedException {
        Platform.runLater(() -> fileManager.fileLabel.setText("read indexes..."));
        if(forge) {
            JsonObject forgeIndexes = new JsonParser().parseUrl(BaseConfig.forgeIndexesUrl);
            JsonObject indexes = new JsonParser().parseUrl(BaseConfig.indexesUrl);
            JsonObject mods = new JsonParser().parseUrl(BaseConfig.modsIndexes);
            JsonObject assetsIndexes = new JsonParser().parseUrl(indexes.get("assetIndex").getAsJsonObject().get("url").getAsString());
            JsonObject notRequirementFiles = new JsonParser().parseUrl(BaseConfig.notRequirementFiles);
            if(!new File(BaseConfig.gameDir).exists()) {
                fileManager.downloadNotOriginalArray(forgeIndexes.get("libraries").getAsJsonArray());
                fileManager.downloadOriginalArray(indexes.get("libraries").getAsJsonArray());
                fileManager.downloadAssets(assetsIndexes);
                fileManager.downloadForgeSingleFiles(indexes);
                fileManager.downloadGCArray(mods.get("mods").getAsJsonArray());
                fileManager.downloadNotRequirementFiles(notRequirementFiles);
                Platform.runLater(() -> fileManager.fileLabel.setText("verify files..."));
                fileManager.verifyAllFiles(forgeIndexes.get("libraries").getAsJsonArray(), indexes.get("libraries").getAsJsonArray(), assetsIndexes, indexes, mods.get("mods").getAsJsonArray());
            } else {
                JsonArray notOriginalArray = forgeIndexes.get("libraries").getAsJsonArray();
                JsonArray originalArray = indexes.get("libraries").getAsJsonArray();
                JsonArray gcArray = mods.get("mods").getAsJsonArray();
                Platform.runLater(() -> fileManager.fileLabel.setText("verify files..."));
                fileManager.verifyAllFiles(notOriginalArray, originalArray, assetsIndexes, indexes, gcArray);
            }
            List<String> command = new StartUpScriptBuilder().buildScript(forgeIndexes.get("libraries").getAsJsonArray(), indexes.get("libraries").getAsJsonArray(), indexes, forgeIndexes, forge);
            ProcessBuilder pb = new ProcessBuilder(command).inheritIO();
            pb.directory(new File(BaseConfig.gameDir));
            Process game = pb.start();
            System.exit(0);
        } else {
            /*JsonObject indexes = new JsonParser().parseUrl(BaseConfig.indexesUrl);
            JsonObject assetsIndexes = new JsonParser().parseUrl(indexes.get("assetIndex").getAsJsonObject().get("url").getAsString());
            JsonObject notRequirementFiles = new JsonParser().parseUrl(BaseConfig.notRequirementFiles);
            if(!new File(BaseConfig.gameDir).exists()) {
                fileManager.downloadOriginalArray(indexes.get("libraries").getAsJsonArray());
                fileManager.downloadAssets(assetsIndexes);
                fileManager.downloadNotRequirementFiles(notRequirementFiles);
                Platform.runLater(() -> fileManager.fileLabel.setText("verify files..."));
            } else {
                JsonArray originalArray = indexes.get("libraries").getAsJsonArray();
                Platform.runLater(() -> fileManager.fileLabel.setText("verify files..."));
            }
            List<String> command = new StartUpScriptBuilder().buildScript(new JsonArray(), indexes.get("libraries").getAsJsonArray(), indexes, new JsonObject(), forge);
            ProcessBuilder pb = new ProcessBuilder(command).inheritIO();
            pb.directory(new File(BaseConfig.gameDir));
            Process game = pb.start();
            System.exit(0);*/
        }
    }
}
