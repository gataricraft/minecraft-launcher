package launcher.game;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.sun.org.apache.xalan.internal.xsltc.dom.SimpleResultTreeImpl;
import launcher.config.Config;
import launcher.game.config.BaseConfig;
import net.lingala.zip4j.core.ZipFile;
import net.lingala.zip4j.exception.ZipException;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class StartUpScriptBuilder {

    private LinkedList<String> classpath;
    private LinkedList<String> natives;

    public List<String> buildScript(JsonArray notOriginalArray, JsonArray originalArray, JsonObject indexes, JsonObject forgeIndexes, boolean forge) throws ZipException, IOException {
        List<String> script = new ArrayList<>();
        script.add("java");
        //script.add("-Dminecraft.client.jar=" + mainPath + packId + "/versions/" + subVersionId + "/" + subVersionId + ".jar");
        script.add("-Duser.home=" + BaseConfig.gameDir);
        script.add("-XX:+UnlockExperimentalVMOptions");
        script.add("-XX:+UseG1GC");
        script.add("-XX:G1NewSizePercent=20");
        script.add("-XX:G1ReservePercent=20");
        script.add("-XX:MaxGCPauseMillis=50");
        script.add("-XX:G1HeapRegionSize=16M");
        //res.add("-XX:MetaspaceSize=1024m");
        script.add("-XX:-UseAdaptiveSizePolicy");
        script.add("-XX:-OmitStackTraceInFastThrow");
        script.add("-Xmn128m");
        script.add("-Xmx" + Config.memoryNow + "m");
        script.add("-Xms" + Config.minMemory + "m");
        script.add("-Dfml.ignoreInvalidMinecraftCertificates=true");
        script.add("-Dfml.ignorePatchDiscrepancies=true");
        script.add("-Djava.library.path=" + BaseConfig.gameDir + "natives/");
        script.add("-cp");
        classpath = new LinkedList<>();
        natives = new LinkedList<>();
        parseNotOriginalArray(notOriginalArray);
        parsetOriginalArray(originalArray);
        classpath.add(BaseConfig.gameDir + BaseConfig.versionsFolder + "/" + BaseConfig.versionId + "/" + BaseConfig.versionId + ".jar");
        unpackNatives();
        StringBuilder stringBuilder = new StringBuilder();
        for(String path : classpath) {
            stringBuilder.append(path);
            stringBuilder.append(";");
        }
        stringBuilder.setLength(stringBuilder.length()-1);
        script.add(stringBuilder.toString());
        script.add(forgeIndexes.get("mainClass").getAsString());
        List<String> args = new GameArgumentsHandler().parseArguments(forgeIndexes.get("minecraftArguments").getAsString(), forge);
        script.addAll(args);
        return script;
    }

    private void parseNotOriginalArray(JsonArray jsonArray) {
        for (JsonElement jsonElement : jsonArray) {
            JsonObject jsonObject = jsonElement.getAsJsonObject();
            String parsedName = StartUpManager.fileManager.parseLibName(jsonObject.get("name").getAsString());
            String fullPath = BaseConfig.librariesDir + parsedName;
            classpath.add(fullPath);
        }
    }

    private void unpackNatives() throws ZipException {
        new File(BaseConfig.gameDir + "natives/").mkdirs();
        for (String aNative : natives) {
            ZipFile zipFile = new ZipFile(aNative);
            zipFile.extractAll(BaseConfig.gameDir + "natives/");
        }
    }

    private void parsetOriginalArray(JsonArray jsonArray) {
        for (JsonElement jsonElement : jsonArray) {
            JsonElement artifactElement = jsonElement.getAsJsonObject().get("downloads").getAsJsonObject().get("artifact");
            JsonElement classifiersElement = jsonElement.getAsJsonObject().get("downloads").getAsJsonObject().get("classifiers");
            if(artifactElement != null) {
                JsonObject artifact = artifactElement.getAsJsonObject();
                String fullPath = BaseConfig.librariesDir + artifact.get("path").getAsString();
                classpath.add(fullPath);
            }
            if(classifiersElement != null) {
                if(classifiersElement.getAsJsonObject().get("natives-windows") != null) {
                    JsonObject classifiers = classifiersElement.getAsJsonObject().get("natives-windows").getAsJsonObject();
                    String fullPath = BaseConfig.librariesDir + classifiers.get("path").getAsString();
                    natives.add(fullPath);
                }
            }
        }
    }
}
