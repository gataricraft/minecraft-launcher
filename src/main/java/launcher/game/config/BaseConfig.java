package launcher.game.config;

public class BaseConfig {
    public static boolean forge;
    public static String packId;
    public static String versionsFolder;
    public static String assetsFolder;
    public static String versionId;
    public static String librariesFolder;
    public static String forgeVersionId;
    public static String gameDir;
    public static String assetDir;
    public static String forgeIndexesUrl;
    public static String librariesRepository;
    public static String indexesUrl;
    public static String assetsRepository;
    public static String assetObjectsDir;
    public static String assetIndexesDir;
    public static String librariesDir;
    public static String assetIndex;
    public static String modsUrl;
    public static String modsIndexes;
    public static String notRequirementFiles;

    public void setConfig(int serverId) {
        switch (serverId) {
            case 0:
                forge = HiTechConfig.forge;
                packId = HiTechConfig.packId;
                versionsFolder = HiTechConfig.versionsFolder;
                librariesFolder = HiTechConfig.librariesFolder;
                assetsFolder = HiTechConfig.assetsFolder;
                versionId = HiTechConfig.versionId;
                forgeVersionId = HiTechConfig.forgeVersionId;
                forgeIndexesUrl = HiTechConfig.forgeIndexesUrl;
                gameDir = HiTechConfig.gameDir;
                assetDir = HiTechConfig.assetDir;
                librariesRepository = HiTechConfig.librariesRepository;
                indexesUrl = HiTechConfig.indexesUrl;
                assetsRepository = HiTechConfig.assetsRepository;
                assetObjectsDir = HiTechConfig.assetObjectsDir;
                assetIndexesDir = HiTechConfig.assetIndexesDir;
                librariesDir = HiTechConfig.librariesDir;
                assetIndex = HiTechConfig.assetIndex;
                modsUrl = HiTechConfig.modsUrl;
                modsIndexes = HiTechConfig.modsIndexes;
                notRequirementFiles = HiTechConfig.notRequirementFiles;
                break;
            case 1:
                forge = HardcoreSurvivalConfig.forge;
                packId = HardcoreSurvivalConfig.packId;
                versionsFolder = HardcoreSurvivalConfig.versionsFolder;
                librariesFolder = HardcoreSurvivalConfig.librariesFolder;
                assetsFolder = HardcoreSurvivalConfig.assetsFolder;
                versionId = HardcoreSurvivalConfig.versionId;
                forgeVersionId = HardcoreSurvivalConfig.forgeVersionId;
                forgeIndexesUrl = HardcoreSurvivalConfig.forgeIndexesUrl;
                gameDir = HardcoreSurvivalConfig.gameDir;
                assetDir = HardcoreSurvivalConfig.assetDir;
                librariesRepository = HardcoreSurvivalConfig.librariesRepository;
                indexesUrl = HardcoreSurvivalConfig.indexesUrl;
                assetsRepository = HardcoreSurvivalConfig.assetsRepository;
                assetObjectsDir = HardcoreSurvivalConfig.assetObjectsDir;
                assetIndexesDir = HardcoreSurvivalConfig.assetIndexesDir;
                librariesDir = HardcoreSurvivalConfig.librariesDir;
                assetIndex = HardcoreSurvivalConfig.assetIndex;
                modsUrl = HardcoreSurvivalConfig.modsUrl;
                modsIndexes = HardcoreSurvivalConfig.modsIndexes;
                notRequirementFiles = HardcoreSurvivalConfig.notRequirementFiles;
                break;
            case 2:
                forge = VanillaConfig.forge;
                packId = VanillaConfig.packId;
                versionsFolder = VanillaConfig.versionsFolder;
                librariesFolder = VanillaConfig.librariesFolder;
                assetsFolder = VanillaConfig.assetsFolder;
                versionId = VanillaConfig.versionId;
                forgeVersionId = VanillaConfig.forgeVersionId;
                forgeIndexesUrl = VanillaConfig.forgeIndexesUrl;
                gameDir = VanillaConfig.gameDir;
                assetDir = VanillaConfig.assetDir;
                librariesRepository = VanillaConfig.librariesRepository;
                indexesUrl = VanillaConfig.indexesUrl;
                assetsRepository = VanillaConfig.assetsRepository;
                assetObjectsDir = VanillaConfig.assetObjectsDir;
                assetIndexesDir = VanillaConfig.assetIndexesDir;
                librariesDir = VanillaConfig.librariesDir;
                assetIndex = VanillaConfig.assetIndex;
                modsUrl = VanillaConfig.modsUrl;
                modsIndexes = VanillaConfig.modsIndexes;
                notRequirementFiles = VanillaConfig.notRequirementFiles;
                break;
        }
    }
}
