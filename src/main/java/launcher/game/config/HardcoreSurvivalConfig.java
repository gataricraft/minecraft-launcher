package launcher.game.config;

import launcher.config.Config;

public class HardcoreSurvivalConfig {
    public final static boolean forge = true;
    public final static String packId = "HardcoreSurvival";
    public final static String versionsFolder = "versions";
    public final static String librariesFolder = "libraries";
    public final static String assetsFolder = "assets";
    public final static String versionId = "1.12.2";
    public final static String assetIndex = "1.12";
    public final static String forgeVersionId = "forge-14.23.5.2768";
    public final static String gameDir = Config.gamePath + packId + "/";
    public final static String assetDir = Config.gamePath + packId + "/" + assetsFolder + "/";
    public final static String assetObjectsDir = assetDir + "objects/";
    public final static String assetIndexesDir = assetDir + "indexes/";
    public final static String librariesDir = gameDir + librariesFolder + "/";
    public final static String indexesUrl = Config.GCrepositoryUrl + packId + "/" + versionsFolder + "/" + versionId + "/" + versionId + ".json";
    public final static String forgeIndexesUrl = Config.GCrepositoryUrl + packId + "/" + versionsFolder + "/" + forgeVersionId + "/" + forgeVersionId + ".json";
    public final static String librariesRepository = "https://libraries.minecraft.net/";
    public final static String assetsRepository = "http://resources.download.minecraft.net/";
    public final static String modsUrl = Config.GCrepositoryUrl + packId + "/mods/";
    public final static String modsIndexes = modsUrl + "mods.json";
    public final static String notRequirementFiles = Config.GCrepositoryUrl + packId + "/files.json";
}
