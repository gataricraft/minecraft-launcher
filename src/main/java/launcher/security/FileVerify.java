package launcher.security;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import javafx.application.Platform;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import launcher.download.DownloadMC;
import launcher.utils.JsonParser;
import launcher.utils.SHA1;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;

public class FileVerify {

    private ProgressBar progress;
    private Label fileLabel;
    private Label progressLabel;
    private String mainUrl = "https://mc.gatari.pw/minecraft/";
    private String mainPath = "C:/GatariCraft/";

    private ArrayList<ArrayList> localArray;
    private ArrayList<ArrayList> globalArray;

    public FileVerify(ProgressBar progress, Label fileLabel, Label progressLabel) {
        this.progress = progress;
        this.fileLabel = fileLabel;
        this.progressLabel = progressLabel;
    }

    public void checkGameFiles(String packId, String subVersionId, String packId1, String subVersionId1, boolean mods) throws IOException, NoSuchAlgorithmException {
        ArrayList<String> noModFolders = new ArrayList<>();
        new File(mainPath + packId).mkdirs();
        File[] noModFiles = new File(mainPath + packId).listFiles();
        File noModVersions = new File(mainPath + packId + "/versions/" + subVersionId + "/" + subVersionId + ".jar");

        for (File noModFile : noModFiles) {
            if (noModFile.isDirectory()) {
                noModFolders.add(noModFile.getName());
            }
        }

        if(noModFolders.contains("assets")) {
            checkAssets(new File(mainPath + packId + "/assets/").listFiles(), packId, subVersionId);
        } else {
            new DownloadMC(progress, fileLabel, progressLabel).downloadAssets(packId, subVersionId);
        }
        if(noModFolders.contains("libraries")) {
            checkLibraries(new File(mainPath + packId + "/libraries/").listFiles(), packId, subVersionId, "libraries");
        } else {
            new DownloadMC(progress, fileLabel, progressLabel).downloadArray(packId, subVersionId, "libraries");
        }
        if(noModFolders.contains("versions")) {
            checkVersions(new File(mainPath + packId + "/versions/").listFiles(), noModVersions, packId, subVersionId);
        } else {
            new DownloadMC(progress, fileLabel, progressLabel).downloadMinecraft(packId, subVersionId, false);
        }

        if(mods) {
            ArrayList<String> modFolders = new ArrayList<>();
            new File(mainPath + packId1).mkdirs();
            File[] modFiles = new File(mainPath + packId1).listFiles();

            for (File modFile : modFiles) {
                if (modFile.isDirectory()) {
                    modFolders.add(modFile.getName());
                }
            }
            if(modFolders.contains("libraries")) {
                checkLibraries(new File(mainPath + packId1 + "/libraries/").listFiles(), packId1, subVersionId1, "libraries");
            } else {
                new DownloadMC(progress, fileLabel, progressLabel).downloadArray(packId1, subVersionId1, "libraries");
            }
            if(modFolders.contains("mods")) {
                checkLibraries(new File(mainPath + packId1 + "/mods/").listFiles(), packId1, subVersionId1, "mods");
            } else {
                new DownloadMC(progress, fileLabel, progressLabel).downloadArray(packId1, subVersionId1, "mods");
            }
        }
    }

    public void checkAssets(File[] folder, String packId, String subVersionId) throws IOException, NoSuchAlgorithmException {
        Platform.runLater(() -> {
            fileLabel.setText("checking assets...");
        });
        localArray = new ArrayList<>();
        globalArray = new ArrayList<>();

        getLocalFiles(folder);
        getGlobalAssets(packId, subVersionId);

        ArrayList<ArrayList> tempArray = (ArrayList<ArrayList>) globalArray.clone();
        ArrayList<ArrayList> tempArray1 = (ArrayList<ArrayList>) localArray.clone();

        for(int i = 0; i < globalArray.size(); i++) {
            ArrayList<String> blankStringArray = new ArrayList<>();
            ArrayList<String> tempStringArray = globalArray.get(i);
            blankStringArray.add(tempStringArray.get(0));
            blankStringArray.add(tempStringArray.get(1));
            blankStringArray.add(tempStringArray.get(2));
            if(localArray.contains(blankStringArray)) {
                tempArray1.remove(blankStringArray);
                tempArray.remove(tempStringArray);
            }
        }
        for(ArrayList<String> file : tempArray1) {
            if(!new File(mainPath + file.get(0)).delete()) System.exit(-100);
        }
        for(ArrayList<String> file :  tempArray) {
            downloadFile(file.get(3), mainPath + file.get(0).replace(file.get(1), ""), file.get(1), Long.parseLong(file.get(2)));
        }

        localArray.clear();
        globalArray.clear();
    }

    private void checkLibraries(File[] folder, String packId, String subVersionId, String arrayName) throws IOException, NoSuchAlgorithmException {
        Platform.runLater(() -> {
            fileLabel.setText("checking " + arrayName + "...");
        });
        localArray = new ArrayList<>();
        globalArray = new ArrayList<>();

        getLocalFiles(folder);
        getGlobalArrayFiles(packId, subVersionId, arrayName);

        ArrayList<ArrayList> tempArray = (ArrayList<ArrayList>) globalArray.clone();
        ArrayList<ArrayList> tempArray1 = (ArrayList<ArrayList>) localArray.clone();

        for(int i = 0; i < globalArray.size(); i++) {
            ArrayList<String> blankStringArray = new ArrayList<>();
            ArrayList<String> tempStringArray = globalArray.get(i);
            blankStringArray.add(tempStringArray.get(0));
            blankStringArray.add(tempStringArray.get(1));
            blankStringArray.add(tempStringArray.get(2));
            if(localArray.contains(blankStringArray)) {
                tempArray1.remove(blankStringArray);
                tempArray.remove(tempStringArray);
            }
        }
        for(ArrayList<String> file : tempArray1) {
            if(!new File(mainPath + file.get(0)).delete()) System.exit(-100);
        }
        for(ArrayList<String> file :  tempArray) {
            String name = file.get(0).split("/")[file.get(0).split("/").length-1];
            downloadFile(file.get(3), mainPath + file.get(0).replace(name, ""), name, Long.parseLong(file.get(2)));
        }

        localArray.clear();
        globalArray.clear();
    }

    private void checkVersions(File[] folder, File file, String packId, String subVersionId) throws IOException, NoSuchAlgorithmException {
        Platform.runLater(() -> {
            fileLabel.setText("checking versions...");
        });
        localArray = new ArrayList<>();

        getLocalFiles(folder);

        JsonObject json = new JsonParser().parseUrl(mainUrl + packId + "/versions/" + subVersionId + "/" + subVersionId + ".json");
        JsonObject temp = json.get("downloads").getAsJsonObject().get("client").getAsJsonObject();
        String path = mainPath + packId + "/versions/" + subVersionId + "/";
        String name = subVersionId + ".jar";

        ArrayList<String> versionGlobal = new ArrayList<>();
        versionGlobal.add(path.replace(mainPath, "") + name);
        versionGlobal.add(temp.get("sha1").getAsString());
        versionGlobal.add(temp.get("size").getAsString());

        for(int i = 0; i < localArray.size(); i++) {
            if(!localArray.get(i).equals(versionGlobal)) {
                if(!new File(mainPath + localArray.get(i).get(0)).delete()) System.exit(-100);
            }
        }

        if(file.exists()) {
            ArrayList<String> versionLocal = new ArrayList<>();
            versionLocal.add(file.getAbsolutePath().replace("\\", "/").replace(mainPath, ""));
            versionLocal.add(SHA1.calcSHA1(file));
            versionLocal.add(String.valueOf(file.length()));

            if(!versionLocal.equals(versionGlobal)) {
                File jarDir = new File(path);
                if(!jarDir.exists()) jarDir.mkdirs();
                downloadFile(temp.get("url").getAsString(), path, name, temp.get("size").getAsLong());
            }
        } else {
            File jarDir = new File(path);
            if(!jarDir.exists()) jarDir.mkdirs();
            downloadFile(temp.get("url").getAsString(), path, name, temp.get("size").getAsLong());
        }

        localArray.clear();
    }

    private void getLocalFiles(File[] folder) throws IOException, NoSuchAlgorithmException {
        for (File file : folder) {
            if(!file.isDirectory()) {
                ArrayList<String> temp = new ArrayList<>();
                temp.add(file.getAbsolutePath().replace("\\", "/").replace(mainPath, ""));
                temp.add(SHA1.calcSHA1(file));
                temp.add(String.valueOf(file.length()));
                localArray.add(temp);
            } else {
                getLocalFiles(file.listFiles());
            }
        }
    }

    private void getGlobalArrayFiles(String packId, String subVersionId, String arrayName) throws IOException, NoSuchAlgorithmException {
        JsonObject json = new JsonParser().parseUrl(mainUrl + packId + "/versions/" + subVersionId + "/" + subVersionId + ".json");
        JsonArray libs = json.get(arrayName).getAsJsonArray();
        for(int i = 0; i < libs.size(); i++) {
            JsonObject downloads = libs.get(i).getAsJsonObject().get("downloads").getAsJsonObject();
            if(downloads.get("artifact") != null) {
                JsonObject temp = downloads.get("artifact").getAsJsonObject();
                String name = temp.get("path").getAsString().split("/")[temp.get("path").getAsString().split("/").length - 1];
                String path = mainPath + packId + "/" + arrayName + temp.get("path").getAsString().replace(name, "");

                ArrayList<String> tempArray = new ArrayList<>();
                tempArray.add(path.replace(mainPath, "") + name);
                tempArray.add(temp.get("sha1").getAsString());
                tempArray.add(temp.get("size").getAsString());
                tempArray.add(temp.get("url").getAsString());
                globalArray.add(tempArray);
            }
            if(downloads.get("classifiers") != null) {
                if(downloads.get("classifiers").getAsJsonObject().get("natives-windows") != null) {
                    JsonObject temp = downloads.get("classifiers").getAsJsonObject().get("natives-windows").getAsJsonObject();
                    String name =  temp.get("path").getAsString().split("/")[temp.get("path").getAsString().split("/").length - 1];
                    String path = mainPath + packId + "/" + arrayName + temp.get("path").getAsString().replace(name, "");

                    ArrayList<String> tempArray = new ArrayList<>();
                    tempArray.add(path.replace(mainPath, "") + name);
                    tempArray.add(temp.get("sha1").getAsString());
                    tempArray.add(temp.get("size").getAsString());
                    tempArray.add(temp.get("url").getAsString());
                    globalArray.add(tempArray);
                }
            }
        }
    }

    private void getGlobalAssets(String packId, String subVersionId) throws IOException {
        JsonObject json = new JsonParser().parseUrl(mainUrl + packId + "/versions/" + subVersionId + "/" + subVersionId + ".json");
        JsonObject assetIndex = json.get("assetIndex").getAsJsonObject();
        JsonObject assetObjects = new JsonParser().parseUrl(assetIndex.get("url").getAsString()).get("objects").getAsJsonObject();
        String[] keys = assetObjects.keySet().toArray(new String[0]);
        for(int i = 0; i < assetObjects.size(); i++) {
            String  hash = assetObjects.get(keys[i]).getAsJsonObject().get("hash").getAsString();
            long  size = assetObjects.get(keys[i]).getAsJsonObject().get("size").getAsInt();
            String path = packId + "/assets/objects/" + hash.substring(0, 2) + "/" + hash;
            ArrayList<String> temp = new ArrayList<>();
            temp.add(path);
            temp.add(hash);
            temp.add(String.valueOf(size));
            temp.add(mainUrl + packId + "/assets/" + keys[i]);
            globalArray.add(temp);
        }
        // Download assetsIndex
        ArrayList<String> temp = new ArrayList<>();
        ArrayList<String> temp1 = new ArrayList<>();
        temp.add(packId +  "/assets/indexes/" + subVersionId + ".json");
        temp.add(assetIndex.get("sha1").getAsString());
        temp.add(String.valueOf(assetIndex.get("size").getAsLong()));
        temp.add(assetIndex.get("url").getAsString());
        globalArray.add(temp);
        //Download logConfig
        JsonObject logConfig = json.get("logging").getAsJsonObject().get("client").getAsJsonObject().get("file").getAsJsonObject();
        temp1.add(packId +  "/assets/log_configs/" + logConfig.get("id").getAsString());
        temp1.add(logConfig.get("sha1").getAsString());
        temp1.add(String.valueOf(logConfig.get("size").getAsLong()));
        temp1.add(logConfig.get("url").getAsString());
        globalArray.add(temp1);

    }

    private void downloadFile(String site, String path, String fileName, long fileSize) throws IOException {
        URL url = new URL(site);
        HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
        BufferedInputStream in = new BufferedInputStream(httpURLConnection.getInputStream());
        new File(path).mkdirs();
        FileOutputStream fos = new FileOutputStream(path + fileName);
        BufferedOutputStream bout = new BufferedOutputStream(fos, 1024);
        byte[] data = new byte[1024];
        long downloadedFileSize = 0;
        int x = 0;
        Platform.runLater(() -> {
            fileLabel.setText("loading " + fileName);
        });
        while ((x = in.read(data, 0, 1024)) >= 0) {
            downloadedFileSize += x;

            final double curretProgress =((((double)downloadedFileSize) / ((double)fileSize)));

            Platform.runLater(() -> {
                progress.setProgress(curretProgress);
                progressLabel.setText(String.valueOf((int)(curretProgress * 100)));
            });

            bout.write(data, 0, x);
        }
        bout.close();
        in.close();
    }
}
