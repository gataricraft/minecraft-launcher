package launcher.ui.controllers;

import com.sun.security.ntlm.Server;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import launcher.utils.Animations;
import launcher.utils.Settings;

import java.io.IOException;
import java.net.ServerSocket;

public class BaseController {

    private Stage stage;
    private static ServerSocket socket;

    @FXML private Pane border;

    private double xOffset;
    private double yOffset;

    protected Animations animationsC;
    protected Settings settingsC;

    BaseController(Stage stage, ServerSocket socket) {
        this.stage = stage;
        this.socket = socket;
        //Set animations class
        animationsC = new Animations();
        //Set settings class
        settingsC = new Settings();
    }

    @FXML
    protected void setDragHandler() {
        border.setOnMousePressed(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                xOffset = stage.getX() - event.getScreenX();
                yOffset = stage.getY() - event.getScreenY();
            }
        });
        border.setOnMouseDragged(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                stage.setX(event.getScreenX() + xOffset);
                stage.setY(event.getScreenY() + yOffset);
            }
        });
    }

    @FXML
    public void closeRequest() {
        System.exit(0);
    }

    @FXML
    public void minimizeRequest() {
        this.stage.setIconified(true);
    }

    protected ServerSocket getServerSocket() {
        if(socket != null) return socket; else return null;
    }
}
