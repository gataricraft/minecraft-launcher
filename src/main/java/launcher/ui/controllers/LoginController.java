package launcher.ui.controllers;

import com.sun.javafx.application.PlatformImpl;
import com.sun.security.ntlm.Server;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import launcher.api.GCAPIClientMapModel;
import launcher.api.GCAPIException;
import launcher.api.GCAPIManager;
import launcher.config.Config;
import launcher.config.User;
import launcher.ui.layouts.LoginPage;
import launcher.ui.layouts.MenuPage;
import launcher.utils.SavePassword;

import java.awt.*;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.Map;

public class LoginController extends BaseController {

    @FXML private Pane formPane;
    @FXML private TextField loginField;
    @FXML private PasswordField passwordField;
    @FXML private Label error;
    @FXML private Hyperlink siteLink;
    @FXML private Label versionLabel;

    public LoginController(Stage stage, ServerSocket socket) {
        super(stage, socket);
    }

    @FXML
    public void initialize() {
        // Set drag event handler to border #Default
        setDragHandler();

        siteLink.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                try {
                    Desktop.getDesktop().browse(new URI("http://mc.gatari.pw/signup"));
                } catch (IOException | URISyntaxException e) {
                    e.printStackTrace();
                }
            }
        });

        versionLabel.setText(Config.launcherVersion);
    }

    @FXML
    public void login() {
        new Thread(() -> {
            formPane.setDisable(true);
            animationsC.animate(400, formPane, 1.0, 0.7, false);

            // Get user
            GCAPIManager apiManager = new GCAPIManager();
            try {
                if(loginField.getText().matches(Config.regex) && passwordField.getText().matches(Config.regex)) {
                    Map<String, String> args = new HashMap<>();
                    args.put(GCAPIClientMapModel.mapLoginKey, loginField.getText());
                    args.put(GCAPIClientMapModel.mapPasswordKey, passwordField.getText());
                    apiManager.manageRequest(Config.apiClientsRequestIndex, args);
                    if(User.user != null) {
                        if(User.user.get("error") == null) {
                            new SavePassword(getServerSocket()).hashPassAndSave(args.get(GCAPIClientMapModel.mapLoginKey), args.get(GCAPIClientMapModel.mapPasswordKey));
                            PlatformImpl.startup(() -> {
                                try {
                                    getServerSocket().close();
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                                LoginPage.stage.close();
                                try {
                                    new MenuPage().start(new Stage());
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            });
                        } else {
                            Platform.runLater(() -> error.setText(LoginControllerErrorsList.unknownUserOrInvalidPassword));
                            animationsC.animate(400, error, 0.0, 1.0, false);
                        }
                    } else {
                        Platform.runLater(() -> error.setText(LoginControllerErrorsList.unknownError));
                        animationsC.animate(400, error, 0.0, 1.0, false);
                    }
                } else {
                    Platform.runLater(() -> error.setText(LoginControllerErrorsList.loginOrPasswordMatchError));
                    animationsC.animate(400, error, 0.0, 1.0, false);
                }
            } catch (GCAPIException | IOException e) {
                e.printStackTrace();
            }

            animationsC.animate(400, formPane, 0.7, 1.0, false);
            formPane.setDisable(false);
        }).start();
    }
}
