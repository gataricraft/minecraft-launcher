package launcher.ui.controllers;

import com.jfoenix.controls.JFXSlider;
import com.jfoenix.controls.JFXToggleButton;
import com.sun.management.OperatingSystemMXBean;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import launcher.config.Config;
import launcher.config.User;
import launcher.game.FileManager;
import launcher.game.StartUpManager;
import launcher.utils.Ping;
import launcher.utils.SettingsException;

import javax.swing.filechooser.FileSystemView;
import java.io.File;
import java.io.IOException;
import java.lang.management.ManagementFactory;
import java.net.ServerSocket;
import java.util.ArrayList;
import java.util.Properties;
import java.util.Timer;
import java.util.TimerTask;

public class MenuController extends BaseController {

    @FXML private AnchorPane anchorPane;
    @FXML private Pane settings;
    @FXML private Pane select;
    @FXML private Pane loading;
    @FXML private Pane border;
    @FXML private Pane serverPane1;
    @FXML private Pane serverPane2;
    @FXML private Pane serverPane3;
    @FXML private Label ram;
    @FXML private Label error;
    @FXML private Label fileLabel;
    @FXML private Label progressLabel;
    @FXML private Label settingsUserLoginLabel;
    @FXML private Button serverButton1;
    @FXML private Button serverButton2;
    @FXML private Button serverButton3;
    @FXML private Button saveButton;
    @FXML private Button leftButton;
    @FXML private Button rightButton;
    @FXML private JFXToggleButton overlayButton;
    @FXML private JFXSlider slider;
    @FXML private ProgressBar progress;
    @FXML private Label hiTechOnline;
    @FXML private Label hiTechPing;
    @FXML private Label hardcoreSurvivalOnline;
    @FXML private Label hardcoreSurvivalPing;
    @FXML private Label vanillaOnline;
    @FXML private Label vanillaPing;
    @FXML private Label menuVersionLabel;
    @FXML private Label loadingVersionLabel;

    private ArrayList<Pane> selectPosition;
    private ArrayList<Button> serverSelectButtons;

    public MenuController(Stage stage, ServerSocket socket) {
        super(stage, socket);
    }

    @FXML
    public void initialize() {
        // Set drag event handler to border #Default
        setDragHandler();

        //Set max memory
        OperatingSystemMXBean mxbean = (OperatingSystemMXBean) ManagementFactory.getOperatingSystemMXBean();
        Config.userMemory = (int) (mxbean.getTotalPhysicalMemorySize()/1024/1024);

        // Set sliders
        slider.setMax(Config.userMemory);
        ram.setText(Config.minMemory + "мб / " + Config.userMemory + "мб");

        // Load settings
        try {
            settingsLoad();
        } catch (IOException e) {
            e.printStackTrace();
        }

        // Add panes in position array
        selectPosition = new ArrayList<>();
        selectPosition.add(serverPane1);
        selectPosition.add(serverPane2);
        selectPosition.add(serverPane3);

        // Add buttons in select array
        serverSelectButtons = new ArrayList<>();
        serverSelectButtons.add(serverButton1);
        serverSelectButtons.add(serverButton2);
        serverSelectButtons.add(serverButton3);

        try {
            hiTechOnline.setText(String.valueOf(new Ping().getServerOnline(0)));
            hiTechPing.setText(String.valueOf(new Ping().pingServer("5.189.130.124")) + "ms");
            hardcoreSurvivalOnline.setText(String.valueOf(new Ping().getServerOnline(1)));
            hardcoreSurvivalPing.setText(String.valueOf(new Ping().pingServer("5.189.130.124")) + "ms");
            vanillaOnline.setText(String.valueOf(new Ping().getServerOnline(2)));
            vanillaPing.setText(String.valueOf(new Ping().pingServer("5.189.130.124")) + "ms");
        } catch (IOException e) {
            e.printStackTrace();
        }

        menuVersionLabel.setText(Config.launcherVersion);
        loadingVersionLabel.setText(Config.launcherVersion);
    }

    @FXML
    public void openSettings() {
        try {
            settingsLoad();
        } catch (IOException e) {
            e.printStackTrace();
        }
        animationsC.openAction(settings);
    }

    @FXML
    public void saveSettings() {
        try {
            settingsC.saveSettings((int)slider.getValue(), overlayButton.isSelected());
        } catch (SettingsException | IOException e) {
            e.printStackTrace();
        }
        saveButton.setText("Сохранено!");
        animationsC.animate(400, saveButton, 1.0, 0.7, false);
        saveButton.setDisable(true);
    }

    @FXML
    public void cancelSettings() {
        animationsC.closeAction(settings);
    }

    @FXML
    public void ramEdit() {
        ram.setText((int)slider.getValue() + "мб / " + Config.userMemory + "мб");
        settingsSaveButtonEdit();
    }

    @FXML
    public void overlayToggle() {
        settingsSaveButtonEdit();
    }

    @FXML
    public void openSelect() {
        animationsC.openAction(select);
    }

    @FXML
    public void cancelSelect() {
        animationsC.closeAction(select);
        new Timer().schedule(new TimerTask() {
            @Override
            public void run() {
                Platform.runLater(() -> {
                    error.setText("");
                });
            }
        }, 400);
    }

    @FXML
    public void play() {
        FileManager fileManager = new FileManager(fileLabel, progressLabel, progress);
        try {
            settingsLoad();
        } catch (IOException e) {
            e.printStackTrace();
        }
        switch (Config.serverId) {
            case -1:
                error.setText("Вы не выбрали сервер!");
                select.setVisible(true);
                animationsC.animate(400, select, 0.0, 1.0, false);
                break;
            case 0:
                loading.setVisible(true);
                animationsC.animate(400, loading, 0.0, 1.0, false);
                fileLabel.setText("launch preparation...");
                new StartUpManager(fileManager).startUpManagerRequest(0);
                break;
            case 1:
                loading.setVisible(true);
                animationsC.animate(400, loading, 0.0, 1.0, false);
                fileLabel.setText("launch preparation...");
                new StartUpManager(fileManager).startUpManagerRequest(1);
                break;
            case 2:
                loading.setVisible(true);
                animationsC.animate(400, loading, 0.0, 1.0, false);
                fileLabel.setText("launch preparation...");
                new StartUpManager(fileManager).startUpManagerRequest(2);
                break;
        }
    }

    @FXML
    public void selectServer1() {
        setServer(0);
    }

    @FXML
    public void selectServer2() {
        setServer(1);
    }

    @FXML
    public void selectServer3() {
        setServer(2);
    }

    @FXML
    public void selectLeft() {
        leftButton.setDisable(true);
        animationsC.animate(400, leftButton, 1.0, 0.7, false);
        Pane tempPane = null;
        Pane nextPane = null;
        int position = -1;
        for (int i = 0; i < selectPosition.size(); i++) {
            if (selectPosition.get(i).isVisible()) {
                tempPane = selectPosition.get(i);
                position = i;
            }
        }
        if(position != -1) {
            if(position == 0) {
                nextPane = selectPosition.get(selectPosition.size() - 1);
            } else {
                nextPane = selectPosition.get(position - 1);
            }
        }
        Pane finalTempPane = tempPane;
        animationsC.animate(400, finalTempPane, 1.0, 0.0, false);
        new Timer().schedule(new TimerTask() {
            @Override
            public void run() {
                finalTempPane.setVisible(false);
            }
        }, 400);
        Pane finalNextPane = nextPane;
        finalNextPane.setVisible(true);
        animationsC.animate(400, finalNextPane, 0.0, 1.0, false);
        animationsC.animate(400, leftButton, 0.7, 1.0, false);
        new Timer().schedule(new TimerTask() {
            @Override
            public void run() {
                leftButton.setDisable(false);
            }
        }, 400);
    }

    @FXML
    public void selectRight() {
        rightButton.setDisable(true);
        animationsC.animate(400, rightButton, 1.0, 0.7, false);
        Pane tempPane = null;
        Pane nextPane = null;
        int position = -1;
        for (int i = 0; i < selectPosition.size(); i++) {
            if (selectPosition.get(i).isVisible()) {
                tempPane = selectPosition.get(i);
                position = i;
            }
        }
        if(position != -1) {
            if(position == selectPosition.size()-1) {
                nextPane = selectPosition.get(0);
            } else {
                nextPane = selectPosition.get(position + 1);
            }
        }
        Pane finalTempPane = tempPane;
        animationsC.animate(400, finalTempPane, 1.0, 0.0, false);
        new Timer().schedule(new TimerTask() {
            @Override
            public void run() {
                finalTempPane.setVisible(false);
            }
        }, 400);
        Pane finalNextPane = nextPane;
        finalNextPane.setVisible(true);
        animationsC.animate(400, finalNextPane, 0.0, 1.0, false);
        animationsC.animate(400, rightButton, 0.7, 1.0, false);
        new Timer().schedule(new TimerTask() {
            @Override
            public void run() {
                rightButton.setDisable(false);
            }
        }, 400);
    }

    private void settingsSaveButtonEdit() {
        if(saveButton.isDisable()) {
            animationsC.animate(400, saveButton, 0.7, 1.0, false);
            new Timer().schedule(new TimerTask() {
                @Override
                public void run() {
                    Platform.runLater(() -> {
                        saveButton.setText("Сохранить");
                        saveButton.setDisable(false);
                    });
                }
            }, 400);
        }
    }

    private void settingsLoad() throws IOException {
        // Set character name
        settingsUserLoginLabel.setText(User.user.get("login"));
        // Load settings
        File settingsFile = new File(FileSystemView.getFileSystemView().getDefaultDirectory().getPath() + "/GatariCraft/settings.properties");
        if(settingsFile.exists()) {
            Properties properties = settingsC.propertiesLoad(settingsFile);
            slider.setValue(Double.parseDouble(properties.getProperty("RAM")));
            Config.memoryNow = Integer.parseInt(properties.getProperty("RAM"));
            ram.setText(Integer.parseInt(properties.getProperty("RAM")) + "мб / " + Config.userMemory + "мб");
            overlayButton.setSelected(Boolean.parseBoolean(properties.getProperty("OVERLAY")));
        }
    }

    private void setServer(int i) {
        serverSelectButtons.get(i).setDisable(true);
        if(Config.serverId == i) {
            serverSelectButtons.get(i).setText("Выбрать сервер");
            animationsC.animate(400, serverSelectButtons.get(i), 0.7, 1.0, false);
            Config.serverId = -1;
        } else {
            serverSelectButtons.get(i).setText("Сервер выбран!");
            animationsC.animate(400, serverSelectButtons.get(i), 1.0, 0.7, false);
            Config.serverId = i;
            for(int j = 0; j < serverSelectButtons.size(); j++) {
                Button tempButton = serverSelectButtons.get(j);
                if(tempButton.getOpacity() == 0.7 && tempButton.getText().equals("Сервер выбран!")) {
                    tempButton.setText("Выбрать сервер");
                    tempButton.setOpacity(1.0);
                }
            }
        }
        serverSelectButtons.get(i).setDisable(false);
    }

}
