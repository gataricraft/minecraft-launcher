package launcher.ui.controllers;

import com.sun.javafx.application.PlatformImpl;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.stage.Stage;
import launcher.api.GCAPIException;
import launcher.config.Config;
import launcher.ui.layouts.LoginPage;
import launcher.ui.layouts.MenuPage;
import launcher.ui.layouts.UpdatePage;
import launcher.utils.LauncherUpdateManager;
import launcher.utils.SavePassword;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.URISyntaxException;
import java.security.NoSuchAlgorithmException;


public class UpdateController extends BaseController {

    @FXML private Label progressLabel;
    @FXML private Label fileLabel;
    @FXML private ProgressBar progress;
    @FXML private Label versionLabel;

    public UpdateController(Stage stage, ServerSocket socket) {
        super(stage, socket);
    }

    @FXML
    public void initialize() {
        // Set drag event handler to border #Default
        setDragHandler();

        versionLabel.setText(Config.launcherVersion);

        new Thread(() -> {
            try {
                new LauncherUpdateManager(progressLabel, fileLabel, progress, getServerSocket()).checkUpdate();
            } catch (IOException | GCAPIException | URISyntaxException | NoSuchAlgorithmException e) {
                e.printStackTrace();
            }
        }).start();
    }
}
