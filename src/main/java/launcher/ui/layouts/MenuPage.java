package launcher.ui.layouts;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import launcher.config.Config;
import launcher.ui.controllers.MenuController;

import java.awt.*;
import java.io.IOException;
import java.net.ServerSocket;

public class MenuPage {

    public static Stage stage;

    public static ServerSocket socket;

    public void start(Stage primaryStage) throws Exception {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/menuPage.fxml"));
        setSocket();
        loader.setController(new MenuController(primaryStage, socket));
        Parent root = loader.load();
        Scene scene = new Scene(root);
        scene.setFill(Color.TRANSPARENT);
        setPrimaryStage(primaryStage);
        primaryStage.setScene(scene);
        primaryStage.initStyle(StageStyle.TRANSPARENT);
        primaryStage.initStyle(StageStyle.UNDECORATED);
        primaryStage.setResizable(false);
        primaryStage.show();
    }

    private void setPrimaryStage(Stage pStage) {
        MenuPage.stage = pStage;
    }

    private void setSocket() {
        try {
            MenuPage.socket = new ServerSocket(Config.socketPort);
        } catch (IOException e) {
            e.printStackTrace();
            System.exit(0);
        }
    }
}
