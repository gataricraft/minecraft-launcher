package launcher.utils;

import javafx.animation.FadeTransition;
import javafx.scene.Node;
import javafx.util.Duration;

public class Animations {
    public void animation(int duration, Object object, double from, double to, boolean reverse) {
        FadeTransition ft = new FadeTransition(Duration.millis(duration), (Node) object);
        ft.setFromValue(from);
        ft.setToValue(to);
        ft.setAutoReverse(reverse);
        ft.play();
    }
}
