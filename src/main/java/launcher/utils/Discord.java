package launcher.utils;

import net.arikia.dev.drpc.DiscordEventHandlers;
import net.arikia.dev.drpc.DiscordRPC;
import net.arikia.dev.drpc.DiscordRichPresence;

public class Discord implements Runnable {
    public static boolean ready = false;

    private static void initDiscord() {
        DiscordEventHandlers handlers = new DiscordEventHandlers.Builder().setReadyEventHandler((user) -> {
            ready = true;
            DiscordRPC.discordUpdatePresence(new DiscordRichPresence.Builder("Logging in").setStartTimestamps(System.currentTimeMillis()/1000).setBigImage("512","GatariCraft").build());
        }).build();
        DiscordRPC.discordInitialize("497463580107472927", handlers, true);
    }

    @Override
    public void run() {
        Runtime.getRuntime().addShutdownHook(new Thread(DiscordRPC::discordShutdown));
        initDiscord();
        while (true) {
            DiscordRPC.discordRunCallbacks();
        }
    }
}
