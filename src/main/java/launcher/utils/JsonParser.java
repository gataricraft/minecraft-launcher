package launcher.utils;

import com.google.gson.JsonObject;

import java.io.IOException;
import java.net.URL;
import java.util.Scanner;

public class JsonParser {
    public JsonObject parseUrl(String urlIn) throws IOException {
        com.google.gson.JsonParser parser = new com.google.gson.JsonParser();
        URL url = new URL(urlIn);
        Scanner scanner = new Scanner(url.openStream());
        StringBuilder stringBuilder = new StringBuilder();
        while (scanner.hasNext()) {
            stringBuilder.append(scanner.nextLine());
        }
        return (JsonObject) parser
                .parse(stringBuilder.toString())
                .getAsJsonObject();
    }

    /*public JsonObject JsonParseObjects(String server, String versionid, String urlT, String object) throws IOException {
        com.google.gson.JsonParser parser = new com.google.gson.JsonParser();
        URL url = new URL(urlT + server + "/versions/" + versionid + "/" + versionid + ".json");
        Scanner scanner = new Scanner(url.openStream());
        StringBuilder stringBuilder = new StringBuilder();
        while (scanner.hasNext()) {
            stringBuilder.append(scanner.nextLine());
        }
        JsonObject jsonObject = (JsonObject) parser
                .parse(stringBuilder.toString())
                .getAsJsonObject().get(object).getAsJsonObject();
        return jsonObject;
    }*/
}
