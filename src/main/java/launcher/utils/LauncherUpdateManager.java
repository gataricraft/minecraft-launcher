package launcher.utils;

import com.google.gson.JsonObject;
import javafx.application.Platform;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import launcher.api.GCAPIException;
import launcher.config.Config;
import launcher.game.DownloadManager;
import launcher.game.config.BaseConfig;
import launcher.ui.layouts.UpdatePage;

import java.io.File;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.URISyntaxException;
import java.security.NoSuchAlgorithmException;

public class LauncherUpdateManager {

    private Label fileLabel;
    private Label progressLabel;
    private ProgressBar progress;

    private ServerSocket serverSocket;

    public LauncherUpdateManager(Label progressLabel, Label fileLabel, ProgressBar progress, ServerSocket socket) {
        this.fileLabel = fileLabel;
        this.progressLabel = progressLabel;
        this.progress = progress;
        this.serverSocket = socket;
    }

    public void checkUpdate() throws IOException, GCAPIException, NoSuchAlgorithmException, URISyntaxException {
        Platform.runLater(() -> {
            fileLabel.setText("checking launcher update...");
        });
        JsonObject json = new JsonParser().parseUrl(Config.GCrepositoryUrl + "launcher/launcher.json");
        File launcher = new File(UpdatePage.class.getProtectionDomain().getCodeSource().getLocation().toURI().getPath());
        if(!json.get("sha1").getAsString().equals(SHA1.calcSHA1(launcher))) {
            update(launcher, json.get("name").getAsString(), json.get("size").getAsLong());
        } else {
            new SavePassword(serverSocket).passwordHandler();
        }
    }

    private void update(File launcher, String fileName, long size) throws IOException {
        String name = launcher.getName();
        String path = launcher.getAbsolutePath().replace(name, "");
        new DownloadManager(fileLabel, progressLabel, progress).downloadFile(Config.GCrepositoryUrl + "launcher/" + fileName, path, launcher.getName(), size);
        ProcessBuilder pb = new ProcessBuilder("java", "-jar", path + launcher.getName());
        pb.directory(new File(path));
        pb.start();
        System.exit(0);
    }


}
