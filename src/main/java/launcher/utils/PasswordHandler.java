package launcher.utils;

import com.google.gson.JsonObject;
import launcher.api.Clients;

import javax.swing.filechooser.FileSystemView;
import java.io.*;

public class PasswordHandler {

    public JsonObject checkSavePassword(File file) throws IOException {
        FileReader fr = new FileReader(file);
        BufferedReader br = new BufferedReader(fr);
        String line;
        if((line = br.readLine()) != null) {
            String[] temp = line.split("@@%%@@");
            if(temp.length == 2) {
                try {
                    return new Clients().getClient(new MD5().decrypt(temp[1]).split("#&#")[1], new MD5().decrypt(temp[0]).split("#&#")[1]);
                } catch (IOException e) {
                    e.printStackTrace();
                    return new JsonObject();
                }
            }
        }
        fr.close();
        return new JsonObject();
    }

    public void hashPassAndSave(String password, String login) throws IOException {
        File dir = new File(FileSystemView.getFileSystemView().getDefaultDirectory().getPath() + "/GatariCraft/");
        File file = new File(FileSystemView.getFileSystemView().getDefaultDirectory().getPath() + "/GatariCraft/save.gc");
        if(!dir.exists()) dir.mkdirs();
        if(!file.exists()) file.createNewFile();
        writeInFile(file, new MD5().encrypt(password) + "@@%%@@" + new MD5().encrypt(login));
    }

    private void writeInFile(File file,String data) throws IOException {
        FileOutputStream fos = new FileOutputStream(file);
        fos.write(data.getBytes(), 0, data.length());
        fos.close();
    }
}
