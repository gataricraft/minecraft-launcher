package launcher.utils;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;

public class Ping {
    public long pingServer(String servername) throws IOException {
        long currentTime = System.currentTimeMillis();
        boolean isPinged = InetAddress.getByName(servername).isReachable(2000);
        currentTime = System.currentTimeMillis() - currentTime;
        return currentTime;
    }

    public int getServerOnline(int serverIndex) throws IOException {
        JsonArray jsonArray = new JsonParser().parseUrlArray("https://mc.gatari.pw:1488/get_servers");
        if(jsonArray.size()-1 >= serverIndex) {
            return jsonArray.get(serverIndex).getAsJsonObject().get("players").getAsJsonObject().get("online").getAsInt();
        } else {
            return -1;
        }
    }
}
