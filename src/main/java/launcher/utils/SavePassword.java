package launcher.utils;

import com.sun.javafx.application.PlatformImpl;
import javafx.stage.Stage;
import launcher.api.GCAPIException;
import launcher.api.GCAPIManager;
import launcher.config.Config;
import launcher.config.User;
import launcher.ui.layouts.LoginPage;
import launcher.ui.layouts.MenuPage;
import launcher.ui.layouts.UpdatePage;

import javax.swing.filechooser.FileSystemView;
import java.io.*;
import java.net.ServerSocket;
import java.util.HashMap;
import java.util.Map;

public class SavePassword {

    private ServerSocket serverSocket;

    public SavePassword(ServerSocket socket) {
        this.serverSocket = socket;
    }

    public void passwordHandler() throws IOException, GCAPIException {
        File file = new File(Config.dataPath + "save.gc");
        if(file.exists()) {
            checkPassword(file);
        } else {
            PlatformImpl.startup(() -> {
                try {
                    serverSocket.close();
                    UpdatePage.stage.close();
                    new LoginPage().start(new Stage());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            });
        }
    }

    public void checkPassword(File file) throws IOException, GCAPIException {
        FileReader fr = new FileReader(file);
        BufferedReader br = new BufferedReader(fr);
        String line;
        if((line = br.readLine()) != null) {
            String[] temp = line.split("@@%%@@");
            MD5 md5 = new MD5();

            GCAPIManager apiManager = new GCAPIManager();
            Map<String, String> args = new HashMap<>();
            args.put("login", md5.decrypt(temp[1]).split("#&#")[1]);
            args.put("password", md5.decrypt(temp[0]).split("#&#")[1]);
            apiManager.manageRequest(Config.apiClientsRequestIndex, args);
            if(User.user != null) {
                if(User.user.get("error") == null) {
                    PlatformImpl.startup(() -> {
                        try {
                            serverSocket.close();
                            UpdatePage.stage.close();
                            new MenuPage().start(new Stage());
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    });
                } else {
                    file.delete();
                    PlatformImpl.startup(() -> {
                        try {
                            serverSocket.close();
                            UpdatePage.stage.close();
                            new LoginPage().start(new Stage());
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    });
                }
            } else {
                PlatformImpl.startup(() -> {
                    try {
                        serverSocket.close();
                        UpdatePage.stage.close();
                        new LoginPage().start(new Stage());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                });
                file.delete();
            }
        } else {
            PlatformImpl.startup(() -> {
                try {
                    serverSocket.close();
                    UpdatePage.stage.close();
                    new LoginPage().start(new Stage());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            });
            file.delete();
        }
        fr.close();
    }

    public void hashPassAndSave(String login, String password) throws IOException {
        File dir = new File(FileSystemView.getFileSystemView().getDefaultDirectory().getPath() + "/GatariCraft/");
        File file = new File(FileSystemView.getFileSystemView().getDefaultDirectory().getPath() + "/GatariCraft/save.gc");
        if(!dir.exists()) dir.mkdirs();
        if(!file.exists()) file.createNewFile();
        writeInFile(file, new MD5().encrypt(password) + "@@%%@@" + new MD5().encrypt(login));
    }

    private void writeInFile(File file,String data) throws IOException {
        FileOutputStream fos = new FileOutputStream(file);
        fos.write(data.getBytes(), 0, data.length());
        fos.close();
    }

    private void closeSocket() throws IOException {
        if(serverSocket != null) {
            serverSocket.close();
        }
    }
}
