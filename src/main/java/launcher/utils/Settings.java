package launcher.utils;

import javax.swing.filechooser.FileSystemView;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;

public class Settings {
    public void saveSettings(int memory, boolean overlay) throws SettingsException, IOException {
        File settingsDir = new File(FileSystemView.getFileSystemView().getDefaultDirectory().getPath() + "/GatariCraft/");
        File settingsFile = new File(FileSystemView.getFileSystemView().getDefaultDirectory().getPath() + "/GatariCraft/settings.properties");
        if(settingsFile.exists()) {
            settingWrite(settingsFile, memory, overlay);
        } else {
            if(!settingsDir.exists()) {
                if(!settingsDir.mkdirs()) throw new SettingsException();
            }
            if(!settingsFile.createNewFile()) throw new SettingsException();
            settingWrite(settingsFile, memory, overlay);
        }
    }

    private void settingWrite(File settingsFile, int memory, boolean overlay) throws IOException {
        Properties properties = new Properties();
        properties.setProperty("RAM", String.valueOf(memory));
        if(overlay) {
            properties.setProperty("OVERLAY", "TRUE");
        } else {
            properties.setProperty("OVERLAY", "FALSE");
        }
        properties.store(new FileOutputStream(settingsFile), "Settings");
    }

    public Properties propertiesLoad(File settingsFile) throws IOException {
        Properties properties = new Properties();
        properties.load(new FileInputStream(settingsFile));
        return properties;
    }
}
